<?php

namespace Database\Seeders;

use App\Models\Releaselog;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReleaseLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $logs = new Releaselog();
        $logs->mergerequest_id = "1";
        $logs->description = "Description";
        $logs->count = "10";
        $logs->created_by = "1";
        $logs->save();
    }
}
