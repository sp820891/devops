<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::select(DB::raw("INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
        (1, 'can access dashboard', 'web', '2023-05-22 03:46:46', '2023-05-22 03:46:46'),
        (2, 'can access merge requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (3, 'can create merge requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (4, 'can view merge requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (5, 'can edit merge requests', 'web', '2023-05-24 00:55:11', '2023-05-24 00:55:11'),
        (6, 'can delete merge requests', 'web', '2023-05-24 00:55:29', '2023-05-24 00:55:29'),
        (7, 'can access support requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (8, 'can create support requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (9, 'can view support requests', 'web', '2023-05-24 00:54:55', '2023-05-24 00:54:55'),
        (10, 'can edit support requests', 'web', '2023-05-24 00:55:11', '2023-05-24 00:55:11'),
        (11, 'can delete support requests', 'web', '2023-05-24 00:55:29', '2023-05-24 00:55:29'),
        (12, 'can comment merge requests', 'web', '2023-05-24 00:56:03', '2023-05-24 00:56:03'),
        (13, 'can access configuration', 'web', '2023-05-22 04:52:04', '2023-05-22 04:52:04'),
        (14, 'can access a project', 'web', '2023-05-22 04:56:52', '2023-05-22 04:56:52'),
        (15, 'can create a project', 'web', '2023-05-22 04:54:55', '2023-05-22 04:54:55'),
        (16, 'can delete a project', 'web', '2023-05-22 04:56:12', '2023-05-22 04:56:12'),
        (17, 'can access sprint', 'web', '2023-05-22 06:08:56', '2023-05-22 06:08:56'),
        (18, 'can create sprint', 'web', '2023-05-22 06:09:29', '2023-05-22 06:09:29'),
        (19, 'can delete sprint', 'web', '2023-05-24 00:39:43', '2023-05-24 00:39:43'),
        (20, 'can access release env', 'web', '2023-05-24 00:40:22', '2023-05-24 00:40:22'),
        (21, 'can create release env', 'web', '2023-05-24 00:41:09', '2023-05-24 00:41:09'),
        (22, 'can delete release env', 'web', '2023-05-24 00:44:31', '2023-05-24 00:44:31'),
        (23, 'can access priority', 'web', '2023-05-24 00:45:07', '2023-05-24 00:45:07'),
        (24, 'can create priority', 'web', '2023-05-24 00:45:30', '2023-05-24 00:45:30'),
        (25, 'can delete priority', 'web', '2023-05-24 00:45:43', '2023-05-24 00:45:43'),
        (26, 'can access deployment status', 'web', '2023-05-24 00:46:18', '2023-05-24 00:46:18'),
        (27, 'can create deployment status', 'web', '2023-05-24 00:46:32', '2023-05-24 00:46:32'),
        (28, 'can delete deployment status', 'web', '2023-05-24 00:46:52', '2023-05-24 00:46:52'),
        (29, 'can access quality status', 'web', '2023-05-24 00:47:17', '2023-05-24 00:47:17'),
        (30, 'can create quality status', 'web', '2023-05-24 00:47:31', '2023-05-24 00:47:31'),
        (31, 'can delete quality status', 'web', '2023-05-24 00:47:44', '2023-05-24 00:47:44'),
        (32, 'can access release logs', 'web', '2023-05-24 00:48:11', '2023-05-24 00:48:11'),
        (33, 'can delete release logs', 'web', '2023-05-24 00:48:35', '2023-05-24 00:48:35'),
        (34, 'can access comment', 'web', '2023-05-24 00:48:11', '2023-05-24 00:48:11'),
        (35, 'can create comment', 'web', '2023-05-24 00:48:24', '2023-05-24 00:48:24'),
        (36, 'can delete comment', 'web', '2023-05-24 00:48:35', '2023-05-24 00:48:35'),
        (37, 'can access status', 'web', '2023-05-24 00:48:11', '2023-05-24 00:48:11'),
        (38, 'can create status', 'web', '2023-05-24 00:48:24', '2023-05-24 00:48:24'),
        (39, 'can delete status', 'web', '2023-05-24 00:48:35', '2023-05-24 00:48:35'),
        (40, 'can access documents from merge request', 'web', '2023-05-24 00:48:11', '2023-05-24 00:48:11'),
        (41, 'can delete documents from merge request', 'web', '2023-05-24 00:48:24', '2023-05-24 00:48:24'),
        (42, 'can access a security', 'web', '2023-05-22 04:57:34', '2023-05-22 04:57:34'),
        (43, 'can access users', 'web', '2023-05-23 23:16:33', '2023-05-23 23:16:33'),
        (44, 'can add users', 'web', '2023-05-23 23:17:09', '2023-05-23 23:17:09'),
        (45, 'can edit users', 'web', '2023-05-23 23:16:51', '2023-05-23 23:16:51'),
        (46, 'can delete users', 'web', '2023-05-23 23:17:29', '2023-05-23 23:17:29'),
        (47, 'can access a roles', 'web', '2023-05-22 04:58:26', '2023-05-22 04:58:26'),
        (48, 'can add roles', 'web', '2023-05-23 23:19:58', '2023-05-23 23:19:58'),
        (49, 'can delete roles', 'web', '2023-05-23 23:20:20', '2023-05-23 23:20:20'),
        (50, 'can access permissions', 'web', '2023-05-23 23:55:37', '2023-05-23 23:55:37'),
        (51, 'can add permissions', 'web', '2023-05-23 23:55:59', '2023-05-23 23:55:59'),
        (52, 'can delete permissions', 'web', '2023-05-23 23:56:18', '2023-05-23 23:56:18'),
        (53, 'can access user based permissions', 'web', '2023-05-23 23:58:39', '2023-05-23 23:58:39'),
        (54, 'can access role based permissions', 'web', '2023-05-23 23:57:31', '2023-05-23 23:57:31')"));
    }
}
