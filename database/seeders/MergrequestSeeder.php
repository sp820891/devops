<?php

namespace Database\Seeders;

use App\Models\Mergrequest;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MergrequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merge = new Mergrequest();
        $merge->project_id = "1";
        $merge->user_id = "2";
        $merge->git_test_link = "git link";
        $merge->git_prod_link = "git link";
        $merge->sql_update_link = "sql link";
        $merge->env_update_link = "env link";
        $merge->depstatus_id = "1";
        $merge->status_id = "1";
        $merge->priority_id = "1";
        $merge->releaseenv_id = "1";
        $merge->sprint_id = "1";
        $merge->artisan_command = "GIS";
        $merge->quality_score = "10";
        $merge->issue_url = "haroob portal  url";
        $merge->test_release_date = fake()->date();
        $merge->prod_release_date = fake()->date();
        $merge->qualitystatus_id = "1";
        $merge->release_by = "1";
        $merge->save();
    }
}
