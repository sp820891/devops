<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supportrequest', function (Blueprint $table) {
            $table->id();
            $table->string('notes')->nullable();
            $table->foreignId('status_id')->constrained('statuses');
            $table->foreignId('project_id')->constrained('projects');
            $table->foreignId('releaseenv_id')->constrained('releaseenvs');
            $table->string('issue_url')->nullable();
            $table->foreignId('priority_id')->constrained('priorities');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supportrequest');
    }
};
