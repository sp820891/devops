@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Priority
@endsection
@section('content')
<div class="card-body">
    <div class="row">
        <h4 class="heading_h4">Priority</h4>
        <div class="offset-lg-10 col-lg-2">
            @can('can create priority')
            <a href="#" data-toggle="modal" data-target="#add-priority"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-plus"></i>
            </a>
            @else
              <a href="#" style="visibility: hidden" data-toggle="modal" data-target="#add-priority"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px; visibility: hidden;
                float: right">
                <i class="fa fa-plus"></i>
            </a>
             @endcan
        </div>
    </div>
    <!-- end col -->
    <!-- BEGIN MODAL -->
    <!-- Modal Add Priority -->
    <div class="modal fade none-border" id="add-priority">
        <div class="modal-dialog">
            <div class="modal-content user-modal">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" id="close"><i
                        class="fa-sharp fa-solid fa-xmark"></i></button>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Priority</label>
                                <input class="form-control form-white" placeholder="Enter Priority" type="text"
                                    name="name" id="name" />
                                <small id="small_priority_name" class="text-danger"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Description</label>
                                <textarea name="description" class="form-control form-white" id="description" cols="30" rows="3"></textarea>
                                <small id="small_priority_des" class="text-danger"></small>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="mb-2 submit-btn">
                    <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                     id="save-priority">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL -->
</div>
<br>
<table  width="100%" class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Created By</th>
            @can('can delete priority')
            <th scope="col">Action</th>
            @endcan
        </tr>
    </thead>
    <tbody id="priority-table">
    </tbody>
</table>
</div>
</div>
{{-- DELETE MODEL START --}}
<div class="modal fade none-border" id="delete-priority">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_priority">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Priority <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deletePriority()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- DELETE MODEL END --}}
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset('assets/js/admin/api/priority.js')}}"></script>
@endsection

