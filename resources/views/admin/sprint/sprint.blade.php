@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Sprints
@endsection
@section('content')
<div class="card-body">
    <div class="row">
        <h4 class="heading_h4">Sprints</h4>
        <div class="offset-lg-10 col-lg-2">
            @can('can create sprint')


            <a href="#" data-toggle="modal" data-target="#add-sprint"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-plus"></i>
            </a>
            @else

            <a href="#" style="visibility: hidden" data-toggle="modal" data-target="#add-sprint"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-plus"></i>
            </a>
            @endcan
        </div>
    </div>
    <!-- end col -->
    <!-- BEGIN MODAL -->
    <!-- Modal Add Category -->
    <div class="modal fade none-border" id="add-sprint">
        <div class="modal-dialog">
            <div class="modal-content user-modal">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" id="close"><i
                        class="fa-sharp fa-solid fa-xmark"></i></button>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Sprint</label>
                                <input class="form-control form-white" placeholder="Enter Sprint" type="number"
                                    name="name" id="name" />
                                <small id="name_small" class="text-danger"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Goal</label>
                                <textarea name="goal" class="form-control form-white" id="goal" cols="30" rows="3"></textarea>
                                <small id="goal_small" class="text-danger"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Start Date</label>
                                <input class="form-control form-white" type="date"
                                    name="start_date" id="start_date" />
                                <small id="start_small" class="text-danger"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">End Date</label>
                                <input class="form-control form-white" type="date"
                                    name="end_date" id="end_date" />
                                <small id="end_small" class="text-danger"></small>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="mb-2 submit-btn">
                    <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                     id="save-sprint">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL -->
</div>

<br>
<table  width="100%" class="table">
    <thead class="thead-light">

        <tr>
            <th scope="col">Id</th>
            <th scope="col">Sprint</th>
            <th scope="col">Start_Date</th>
            <th scope="col">End_Date</th>
            <th scope="col">Goal</th>
            <th scope="col">Owner</th>
            @can('can delete sprint')
            <th scope="col">Action</th>
            @endcan
        </tr>
    </thead>
    <tbody id="sprint-table">
    </tbody>
</table>
</div>
</div>
{{-- DELETE MODEL START --}}
<div class="modal fade none-border" id="delete-sprint">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true"><i
                    class="fa-sharp fa-solid fa-xmark"></i></button>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_sprint">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Sprint <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deleteSprint()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- DELETE MODEL END --}}
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset('assets/js/admin/api/sprint.js')}}"></script>
@endsection
