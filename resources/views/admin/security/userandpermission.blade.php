<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- theme meta -->
    <meta name="theme-name" content="focus" />
    <title>{{auth()->user()->roles->pluck('name')->first()}} . User Details</title>
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
    <!-- Styles -->
    <link href="{{ asset('assets/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/weather-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/modelstyle.css') }}">
    <link rel="stylesheet" href="{{asset('assets/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{asset("assets/css/lib/toastr/toastr.min.css")}}">
    <!--font cdn-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <style>
         .card {
            overflow: auto;
        }
    </style>
</head>

<body>
    @include('admin.layouts.sidebar')
    <!-- /# sidebar -->
    @include('admin.layouts.navbar')

    {{-- USER VIEW --}}

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <section id="main-content">
                    <div class="col-lg-12">
                        <div class="card" width="71rem" style="height: 38.5rem;">
                            <div class="d-flex ">
                                <div>
                                    <h4>User Details</h4>
                                </div>
                                <div class="ml-auto pl-2">
                                    <h5><a href="{{route('admin.users')}}" class="btn btn-sm btn-primary btn-block waves-effect waves-light" style="width: 35px;
                                        float: right"><i class="fa fa-arrow-left"></i>
                                        </a></h5>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">First Name</label>
                                    <h5>{{$user->first_name}}</h5>
                                </div>
                                <div class="col-md-6 pl-0"> <label class="control-label">Last Name</label>
                                    <h5>{{$user->last_name}}</h5>
                                </div>
                                <div class="col-md-6"> <label class="control-label">Email</label>
                                    <h5>{{$user->email}}</h5>
                                </div>
                                <div class="col-md-6 pl-0">
                                    <label class="control-label">Gender</label>
                                    <h5>{{$user->gender}}</h5>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Role</label>
                                    <h5>{{$user->roles->pluck('name')->first()}}</h5><br>
                                        <label class="control-label">Role Based Permissions</label>
                                        <div class="stat-widget-one">
                                            <div class="stat-content dib">
                                                <div class="row">
                                                <div class="col-md-12 p-0 text-dark">
                                                @forelse ($Permissions as $permission)
                                                    <i class="fa-sharp fa-light fa-square text-primary"></i> {{$permission->name}}
                                                @empty
                                                    No Permissions is Assigned For This Role.
                                                @endforelse
                                                </div>
                                                </div>
                                            </div>
                                        </div><br>
                                        <label class="control-label">User Based Permissions</label>
                                        <div class="stat-widget-one">
                                            <div class="stat-content dib">
                                                <div class="row" id="userBasedPermissions"></div>
                                            </div>
                                        </div>
                                </div>
                                    <div class="col-md-6 pl-0">
                                        <label class="control-label">All Permissions</label>
                                        <div class="stat-widget-one">
                                            <div class="stat-content dib">
                                                <input type="hidden" id="user_id" value="{{$user->id}}">
                                                <div class="row">
                                                    @foreach ($allPermissions as $permission)
                                                    <div class="col-md-6 pt-0 pb-0">
                                                        <input style="cursor:pointer;" class="form-check-input" type="checkbox" value="{{$permission->name}}"
                                                            id="permission_check{{$permission->id}}" name="permission_check">
                                                        <label style="cursor:pointer;" class="form-check-label" for="permission_check{{$permission->id}}">
                                                            <p>{{$permission->name}}</p>
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="button"
                                            onclick="update_permission('{{$user->id}}')" id="update_btn" class="btn btn-primary float-right waves-effect waves-light save-category">Update</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </section>

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="{{ asset('assets/js/lib/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/jquery.nanoscroller.min.js') }}"></script>
    <!-- nano scroller -->
    <script src="{{ asset('assets/js/lib/menubar/sidebar.js') }}"></script>
    <script src="{{ asset('assets/js/lib/preloader/pace.min.js') }}"></script>
    <!-- sidebar -->

    <script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <!-- bootstrap -->

    <script src="{{ asset('assets/js/lib/calendar-2/moment.latest.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.init.js') }}"></script>


    <script src="{{ asset('assets/js/lib/weather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/weather/weather-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/chartist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/sparkline.init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('assets/js/dashboard2.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{asset('assets/js/admin/user&permissions.js')}}"></script>
    <script src="{{asset("assets/js/lib/toastr/toastr.min.js")}}"></script>
    <script src="{{asset('assets/js/sweetalert2.min.js')}}"></script>
</body>

</html>
