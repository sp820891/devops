@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Roles
@endsection
@section('content')
<div class="card-body">
    <div class="row">
        <h4 class="heading_h4">Roles</h4>
        <div class="offset-lg-10 col-lg-2">
            @can('can add roles')


            <a href="#" data-toggle="modal" data-target="#add-role"
            class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
            float: right">
            <i class="fa fa-plus"></i>
            </a>
            @else
            <a href="#" data-toggle="modal" data-target="#add-role"
            class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px; visibility: hidden;
            float: right">
            <i class="fa fa-plus"></i>
            </a>
            @endcan
        </div>
    </div><br>
    <!-- end col -->
    <!-- BEGIN MODAL -->
    <!-- Modal Add Role -->
    <div class="modal fade none-border" id="add-role">
        <div class="modal-dialog">
            <div class="modal-content user-modal">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" id="close"><i
                        class="fa-sharp fa-solid fa-xmark"></i></button>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Name</label>
                                <input class="form-control form-white" placeholder="Enter Role Name" type="text"
                                    id="name" />
                                <small class="text-danger" id="name_small"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Guard Name</label>
                                <input type="text" id="guard_name" placeholder="Enter Guard Name" class="form-control form-white">
                                <small class="text-danger" id="guard_small"></small>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="submit-btn mb-3">
                    <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                       id="saveRole">Save</button>
                </div>
            </div>
        </div>
    </div>
<!-- END MODAL -->
<!-- BEGIN MODAL -->
<!-- Modal Delete Role -->
<div class="modal fade none-border" id="delete-role">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" class="close_btn"><i
                    class="fa-sharp fa-solid fa-xmark"></i></button>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_role">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Role <span id="role_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deleteRole()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
</div>

<table  width="100%" class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Role</th>
            <th scope="col">Gaurd Name</th>
            @canany(['can access role based permissions', 'can delete roles'])
            <th scope="col">Action</th>
            @endcanany
        </tr>
    </thead>
    <tbody id="roles_table">
    </tbody>
</table>
</div>
</div>
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset('assets/js/admin/role.js')}}"></script>
@endsection

