@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Users
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <h4 class="heading_h4">Users</h4>
            <div class="offset-lg-10 col-lg-2">
                @can('can add users')
                <button data-toggle="modal" data-target="#add-user"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-user-plus"></i>
                </button>
                @else
                <button style="visibility: hidden;" data-toggle="modal" data-target="#add-user"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-user-plus"></i>
                </button>
                @endcan
            </div>
        </div>
        <!-- end col -->
        <!-- BEGIN MODAL -->
        <!-- Modal Add User -->
        <div class="modal fade none-border" id="add-user">
            <div class="modal-dialog">
                <div class="modal-content user-modal">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" id="close"><i
                            class="fa-sharp fa-solid fa-xmark"></i></button>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">First Name</label>
                                    <input class="form-control form-white" placeholder="Enter first name" type="text"
                                        id="first_name" />
                                        <small class="text-danger" id="fname_small"></small>
                                        <br>
                                </div>
                                <div class="col-md-6"> <label class="control-label">Last Name</label>
                                    <input class="form-control form-white" placeholder="Enter last name" type="text"
                                        id="last_name" />
                                        <small class="text-danger" id="lname_small"></small>
                                        <br>
                                </div>
                                <div class="col-md-12"> <label class="control-label">Email</label>
                                    <input class="form-control form-white" placeholder="Enter email" type="text"
                                        id="email" />
                                        <small class="text-danger" id="email_small"></small>
                                        <br>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Gender</label><br>
                                    <input class="control-label" type="radio"
                                        name="gender" value="Male" id="gender">
                                    <label class="form-check-label" for="gender">
                                        Male
                                    </label>&nbsp;
                                    <input class="control-label" type="radio"
                                        name="gender" value="Female" id="gender">
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label><br>
                                    <small class="text-danger" id="gender_small"></small>
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Role</label>
                                                <select class="form-control form-white"
                                                    data-placeholder="Choose a role..."
                                                    id="role_as">
                                                    <option selected value="">__</option>
                                                    @forelse ($roles as $role)
                                                      <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @empty
                                                       <option value="">No Roles Found</option>
                                                    @endforelse
                                                </select>
                                    <small class="text-danger" id="role_small"></small>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Password</label>
                                    <input class="form-control form-white" placeholder="Enter Password" type="password"
                                        id="password" />
                                        <small class="text-danger" id="password_small"></small>
                                        <br>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Confirm Password</label>
                                    <input class="form-control form-white" placeholder="Enter Password" type="password"
                                        id="password_confirmation" /><br>
                                </div>
                                <div class="submit-btn">
                                    <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                                        id="saveUser">Save</button>
                                </div>

                            </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- END MODAL -->
<br>

<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th>User Id</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Role</th>
            @canany(['can access user based permissions','can edit users','can delete users'])
              <th>Action</th>
            @endcanany
        </tr>
    </thead>
    <tbody id="users_table">
    </tbody>
    </table>
    </div>
</div>
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<!-- BEGIN MODAL -->
<!-- Modal Edit User -->
<div class="modal fade none-border" id="edit-user">
    <div class="modal-dialog">
        <div class="modal-content user-modal">
            <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true"><i
                    class="fa-sharp fa-solid fa-xmark"></i></button>
            <div class="modal-body">
                <form>
                    <input type="hidden" id="id_edit_user">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">First Name</label>
                            <input class="form-control form-white" placeholder="Enter first name" type="text"
                                id="first_name_edit" />
                                <small class="text-danger" id="fname_small_edit"></small>
                                <br>
                        </div>
                        <div class="col-md-6"> <label class="control-label">Last Name</label>
                            <input class="form-control form-white" placeholder="Enter last name" type="text"
                                id="last_name_edit" />
                                <small class="text-danger" id="lname_small_edit"></small>
                                <br>
                        </div>
                        <div class="col-md-12"> <label class="control-label">Email</label>
                            <input class="form-control form-white" placeholder="Enter email" type="text"
                                id="email_edit" />
                                <small class="text-danger" id="email_small_edit"></small>
                                <br>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Gender</label><br>
                            <input class="control-label" type="radio"
                                name="gender_edit" value="Male" id="gender_edit_male" >
                            <label class="form-check-label" for="gender_edit_male">
                                Male
                            </label>
                            <input class="control-label" type="radio"
                                name="gender_edit" value="Female" id="gender_edit_female">
                            <label class="form-check-label" for="gender_edit_female">
                                Female
                            </label><br>
                            <small class="text-danger" id="gender_small_edit"></small>
                            <br>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Role</label>
                                        <select class="form-control form-white"
                                            data-placeholder="Choose a role..."
                                            id="role_as_edit">
                                            <option selected value="">__</option>
                                            @forelse ($roles as $role)
                                                <option value="{{$role->name}}" id="roles_option{{$role->id}}">{{$role->name}}</option>
                                            @empty
                                                <option value="">No Roles Found</option>
                                            @endforelse
                                        </select>
                            <small class="text-danger" id="role_small_edit"></small>
                        </div>
                        <div class="update-btn mt-3">
                            <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                                id="updateUser">Update</button>
                        </div>

                    </div>
            </div>
            </form>
        </div>

    </div>
</div>
</div>
<!-- END MODAL -->
<!-- BEGIN MODAL -->
<!-- Modal Delete User -->
<div class="modal fade none-border" id="delete-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_user">
                        <input type="hidden" id="deleted_user_email">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete User <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light"
                onclick="deleteUser()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
    <script src="{{asset("assets/js/admin/user.js")}}"></script>
@endsection
