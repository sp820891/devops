<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <ul>
                <div class="logo">
                    <a>
                        <svg style="margin-right: 12px;" height="30" viewBox="-20.21 56.72 333.89 177.33" width="200" xmlns="http://www.w3.org/2000/svg"><g fill="#f2f4f7"><path d="m93.09 176.17 20.91 1.54v17.86s-39.07 38.48-96-7.08h19.4l2.15-17.55s22.14 22.47 53.54 5.23z"/><path d="m22.28 93.66s-42.49 39.56-7.7 90.21l19.42 1.85 1.85-18.78s-17.85-29.25 5.21-52.34l-20.68-1.6z"/><path d="m26.9 90.89s53-34.17 93.29 7.08l-17.86-1.23-1.85 20s-20.32-24.62-54.18-5.53l-20.63-.92z"/><path d="m105.1 121.68 1.23-20.63 18.78 1.85 56.65 63.11h-19.39l-1.85 17.55z"/></g><g fill="#459ed9"><path d="m98.33 172.79 71.73-79.13 19.09 1.85-1.23 19.4-70.5 78.5.3-20.32z"/><path d="m172.83 90.81 19.86 1.62-1.38 18.94s29.32-18.71 50.8 1.15l21 .92 1.89-18.44s-32.82-37-92.17-4.19z"/><path d="m245.11 115.52 21 1.23 1.62-18.55s45.95 51.19-3.7 95.21l-20.31-1.41 1.85-21s27.48-30.77-.46-55.48z"/><path d="m186.69 170.94-21.21-1.85-1.19 18.71s37.64 43.41 94.44 8.77l-19.4-1.57 1.85-20.09s-24.71 19.81-54.49-3.97z"/></g></svg>
                        <span style="position: relative; left: -11px;">{{auth()->user()->roles->pluck('name')->first()}}</span>
                    </a>
                </div>
                @can('can access dashboard')
                <li><a href="{{ route('admin.dashboard') }}"><i class="ti-home"></i> Dashboard </a></li>
                @endcan
                @can('can access merge requests')
                <li><a href="{{ route('admin.mergerequest') }}"><i class="ti-link"></i>Merge Requests</a></li>
                @endcan
                 @can('can access release logs')
                 <li><a href="{{ route('admin.releaselog') }}"><i class="ti-write"></i> Release Logs</a></li>
                 @endcan
                @can('can access support requests')
                <li><a href="{{ route('admin.supportrequest') }}"><i class="ti-support"></i>Support Requests</a></li>
                @endcan
                @can('can access configuration')
                <li><a class="sidebar-sub-toggle"><i class="ti-settings"></i> Configuration
                        <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="{{ route('admin.project') }}">Projects</a></li>
                        <li><a href="{{ route('admin.sprint') }}">Sprints</a></li>
                        <li><a href="{{ route('admin.releaseenv') }}">Release Env</a></li>
                        <li><a href="{{ route('admin.priority') }}">Priority</a></li>
                        <li><a href="{{ route('admin.status') }}">Status</a></li>
                    </ul>
                </li>
                @else
                @can('can access a project')
                    <li><a href="{{ route('admin.project') }}"><i class="ti-desktop"></i> Projects</a></li>
                @endcan
                @can('can access sprint')
                    <li><a href="{{ route('admin.sprint') }}"><i class="ti-calendar"></i> Sprints</a></li>
                @endcan
                @can('can access release env')
                    <li><a href="{{ route('admin.releaseenv') }}"><i class="ti-settings"></i> Release Env</a></li>
                @endcan
                @can('can access priority')
                    <li><a href="{{ route('admin.priority') }}"><i class="ti-layers"></i> Priority</a></li>
                @endcan
                @can('can access status')
                    <li><a href="{{ route('admin.status') }}"><i class="ti-bar-chart"></i> Status</a></li>
                @endcan
                @endcan
                @can('can access a security')
                <li><a class="sidebar-sub-toggle"><i class="ti-lock"></i> Security
                    <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                <ul>
                    <li><a href="{{ route('admin.users') }}">Users</a></li>
                    <li><a href="{{ route('admin.permissions') }}">Permissions</a></li>
                    <li><a href="{{ route('admin.role') }}">Roles</a></li>
                </ul>
                </li>
                @else
                @can('can access users')
                    <li><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> Users</a></li>
                @endcan
                @can('can access permissions')
                    <li><a href="{{ route('admin.permissions') }}"><i class="ti-unlock"></i> Permissions</a></li>
                @endcan
                @can('can access a roles')
                    <li><a href="{{ route('admin.role') }}"><i class="ti-id-badge"></i> Roles</a></li>
                @endcan
                @endcan
                <li><a href="{{ url('logout') }}"><i class="ti-power-off"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>
