<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
@livewireStyles()
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
               <div style="position: absolute;right: 110px; top: 13px;">
                    @if(auth()->user()->profile_picture != NULL)
                    <img src="{{asset(auth()->user()->profile_picture)}}" alt="profile_picture" height="35px" width="35px" style="border-radius: 50px">
                    @else
                    <img src="{{asset('profile_picture/blank.png')}}" alt="profile_picture" height="35px" width="35px" style="border-radius: 50px">
                    @endif
               </div>
                <div class="float-right">
                    <div class="header-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownMenu">
                        @livewire('notification')
                        <div class="dropdown-menu dropdown-menu-notify" aria-labelledby="dropdownMenu">
                            <h6 class="dropdown-header">Recent Notifications</h6>
                            <div id="merg-notify"></div>
                        </div>
                    </div>
                    <div class="dropdown dib">
                        <div class="dropdown">
                            <span class="user-avatar text-dark dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{auth()->user()->first_name}}</span>
                            <div class="dropdown-menu dropdown-menu-user" aria-labelledby="dropdownMenu2">
                              <a class="dropdown-item profile_hover" href="{{route('profile.edit')}}">&nbsp;&nbsp;&nbsp;<i class="ti-user"></i>&nbsp;&nbsp;Profile</a>
                              <a class="dropdown-item logout_hover" href="{{route('logout')}}">&nbsp;&nbsp;&nbsp;<i class="ti-power-off"></i>&nbsp;&nbsp;Logout</a>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@livewireScripts()
<script>
    $('.ti-bell').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "get",
            url: "{{url('merg-notify')}}",
            success: function (response) {
                $('#merg-notify').html("");
                $.each(response.merg, function (i, val) {
                if (val.username != null) {
                   var html ='<a class="dropdown-item" href="{{ URL("viewmergecomment/") }}/'+val.merg_id+'">\
                                                <div class="notification-content">\
                                                    <small class="notification-timestamp pull-right">\
                                                        </small>\
                                                    <div class="notification-heading">'+val.username+'</div>\
                                                    <div class="notification-text">created a mergrequest of id: '+val.merg_id+' </div>\
                                                </div>\
                                     </a>';
                                $('#merg-notify').append(html);

                } else {
                    var  html ='<a class="dropdown-item" href="{{ URL("viewmergecomment/") }}/'+val.merg_id+'">\
                                                <div class="notification-content">\
                                                    <small class="notification-timestamp pull-right">\
                                                        </small>\
                                                        <a href="{{ URL("viewmergecomment/") }}/'+val.merg_id+'"><div class="notification-heading">'+val.rejected_by+'</div></a>\
                                                    <div class="notification-text">rejected the mergrequest having id: '+val.merg_id+' </div>\
                                                </div>\
                                     </a>';
                                $('#merg-notify').append(html);
                }
                });

            }
        });
        });
</script>
