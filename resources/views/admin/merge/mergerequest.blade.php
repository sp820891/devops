<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- theme meta -->
    <meta name="theme-name" content="focus" />
    <title>{{ auth()->user()->roles->pluck('name')->first() }} . Merge Requests</title>
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
    <!-- Styles -->
    <link href="{{ asset('assets/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/weather-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/modelstyle.css') }}">
    <script src="{{ asset('assets/js/sweetalert2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/toastr/toastr.min.css') }}">
    <!--font cdn-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>

<body>
    @include('admin.layouts.sidebar')
    <!-- /# sidebar -->
    @include('admin.layouts.navbar')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="p-2 d-flex">
                                    <div>
                                        @can('can create merge requests')
                                            <a href="#" data-toggle="modal" data-target="#addmerge"
                                                class="btn  btn-sm badge-primary btn-block waves-effect waves-light"
                                                style="width: 100px;">
                                                <i class="ti-plus"></i> New
                                            </a>
                                        @endcan
                                    </div>
                                    {{-- <div class="pl-3">
                                        @can([
    'can view merge requests',
    'can edit merge requests',
    'can delete merge
                                            requests',
])
                                            <div class="dropdown">
                                                <button
                                                    style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa-solid fa-ellipsis" title="Options"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" id="view_mergerequests"
                                                        style="cursor:pointer;">View</a>
                                                    <a class="dropdown-item" data-toggle="modal" id="edit-merge"
                                                        style="cursor:pointer;"> Edit</a>
                                                    <a class="dropdown-item" id="delete-merge" style="cursor:pointer;">
                                                        Delete </a>
                                                </div>
                                            </div>
                                        @elsecan(['can view merge requests','can edit merge requests'])
                                            <div class="dropdown">
                                                <button
                                                    style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa-solid fa-ellipsis" title="Options"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" id="view_mergerequests"
                                                        style="cursor:pointer;">View</a>
                                                    <a class="dropdown-item" data-toggle="modal" id="edit-merge"
                                                        style="cursor:pointer;"> Edit</a>
                                                </div>
                                            </div>
                                        @elsecan(['can edit merge requests','can delete merge requests'])
                                            <div class="dropdown">
                                                <button
                                                    style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa-solid fa-ellipsis" title="Options"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" data-toggle="modal" id="edit-merge"
                                                        style="cursor:pointer;"> Edit</a>
                                                    <a class="dropdown-item" id="delete-merge" style="cursor:pointer;">
                                                        Delete </a>
                                                </div>
                                            </div>
                                        @elsecan(['can view merge requests','can delete merge requests'])
                                            <div class="dropdown">
                                                <button
                                                    style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa-solid fa-ellipsis" title="Options"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" id="view_mergerequests"
                                                        style="cursor:pointer;">View</a>
                                                    <a class="dropdown-item" id="delete-merge" style="cursor:pointer;">
                                                        Delete </a>
                                                </div>
                                            </div>
                                        @elsecan('can view merge requests')
                                            <a id="view_mergerequests" class="btn  btn-sm btn-warning text-light"
                                                style="width: 100px;">
                                                <i class="ti-eye"></i> View
                                            </a>
                                        @elsecan('can edit merge requests')
                                            <a data-toggle="modal" id="edit-merge"
                                                class="btn  btn-sm btn-success text-light" style="width: 100px;">
                                                <i class="ti-pencil-alt"></i> Edit
                                            </a>
                                        @elsecan('can delete merge requests')
                                            <a id="delete-merge" class="btn  btn-sm btn-danger text-light"
                                                style="width: 100px;">
                                                <i class="ti-trash"></i> Delete
                                            </a>
                                        @endcan
                                    </div> --}}
                                    <div class="heading_h5">
                                        <h3>Merge Requests</h3>
                                    </div>
                                </div>

                                <br>

                                <div class="card-body" style="max-height:35.5rem; overflow:auto;">

                                    <table class=" table table-hover" style="width:77.9rem">
                                        <thead style="background: rgb(231, 228, 228)">
                                            <tr>
                                                <th scope="col"><input type="checkbox" id="selectid"></th>
                                                <th scope="col">Project</th>
                                                <th scope="col">Issue</th>
                                                <th scope="col">Git Test Link</th>
                                                <th scope="col">Git Prod Link</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="merge-table">
                                            <td><input type="checkbox" id="selectid"></td>
                                            <td>GIS</td>
                                            <td>https://teams.haroob.com/issues/6317</td>
                                            <td>https://gitlab.com/sp820891/devops</td>
                                            <td>https://gitlab.com/sp820891/devops</td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#approvemerge"
                                                    class="badge badge-success"> <i class="ti-check"></i></a>&nbsp;&nbsp;
                                                <a href="#"  data-toggle="modal" data-target="#rejectmerge" class=" badge badge-danger"> <i class="ti-close"></i> </a>
                                            </td>
                                        </tbody>
                                    </table>

                                </div>

                            </div>


                        </div>
                        @include('admin.audit_log.auditLog')
                </section>
            </div>
        </div>
    </div>
    </div>


    {{-- MERGE INSERT MODEL START --}}

    <div class="modal fade none-border" id="addmerge">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h4 class="modal-title">Merge Request</h4>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark" style="border: none; background: none;">
                    </button>
                </div>
                <div class="modal-body">

                    <form method="post" enctype="multipart/form-data" name="merge_send" id="merge_send">
                        <div class="mb-3 row">

                            <div class="col-sm-6 ">
                                <i class="ti-link"> <label for="issueurl">Issue Url </label> </i>
                                <input type="text" class="form-control text-blue" id="issueurl"
                                    name="issueurl" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 ">
                                <i class="ti-check-box"> <label for="priority"> Priority</label> </i>
                                <select class="form-control  text-blue "id="priority" name="priority">
                                    <option class=" form-control text-blue">Medium </option>
                                </select>
                            </div>

                            <div class="col-sm-6  ">
                                <i class="ti-link"> <label for="gitlinktest"> Git Test Link </label> </i>
                                <input type="text" class="form-control  text-blue " id="gitlinktest"
                                    name="gitlinktest" placeholder="Enter a URL">
                            </div>
                            <div class="col-sm-6  ">
                                <i class="ti-link"> <label for="gitlinkprod"> Git Prod Link </label> </i>
                                <input type="text" class="form-control  text-blue " id="gitlinkprod"
                                    name="gitlinkprod" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 ">
                                <i class="ti-link"> <label for="sqllink">Sql Update Link </label> </i>
                                <input type="text" class="form-control  text-blue " id="sqllink"
                                    name="sqllink" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 ">
                                <i class="ti-link"> <label for="envlink">Env Update Link </label> </i>
                                <input type="text" class="form-control   text-blue" id="envlink"
                                    name="envlink" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 ">
                                <i class="ti-check-box"> <label for="releaseenv"> Release Env</label> </i>
                                <select class="form-control  text-blue "id="releaseenv" name="releaseenv">
                                    <option class=" form-control text-blue ">Testing </option>
                                    <option class=" form-control text-blue ">Production </option>
                                </select>
                            </div>
                            <div class="col-sm-6 ">
                                <i class="ti-check-box"> <label for="status"> Status</label> </i>
                                <select class="form-control  text-blue "id="status" name="status">
                                    <option class=" form-control text-blue ">Test Release Request</option>
                                    <option  class=" form-control text-blue ">Prod Release Request</option>

                                </select>
                            </div>


                            <div class="col-sm-6  ">
                                <i class="ti-align-left"> <label for="artisancommand">Artisan Command</label></i>
                                <textarea class="form-control text-blue" id="artisancommand" rows="6" style="height:75px"></textarea>
                            </div>

                            <div class="col-sm-6   "style="top:30px">
                                <i class="ti-clip"><label> Attachments</label></i>
                                <label for="formFile" class="form-control text-blue "> <i class="ti-clip">Add
                                        Attachment</i></label>
                                <input class="form-control " type="file" id="formFile" name="file[]" multiple
                                    style="display: none">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                id="merge-close" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary"
                                name="save-merge"id="">Save</button>
                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>
    {{-- MERGE INSERT MODEL END --}}

    {{-- MERGE EDIT MODEL START --}}

    <div class="modal fade none-border" id="editmerge">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title ">Merge Request</h5>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark" style="border: none; background: none;">
                    </button>
                </div>
                <div class="modal-body">

                    <form method="post" enctype="multipart/form-data" name="merge_send" id="merge_send">
                        <div class="mb-3 row">

                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="issueurl">Issue Url </label> </i>
                                <input type="text" class="form-control text-secondary" id="issueurl"
                                    name="issueurl" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6  text-primary">
                                <i class="ti-link"> <label for="gitlinktest"> Git Test Link </label> </i>
                                <input type="text" class="form-control  text-secondary " id="gitlinktest"
                                    name="gitlinktest" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6  text-primary">
                                <i class="ti-link"> <label for="gitlinkprod"> Git Pro Link </label> </i>
                                <input type="text" class="form-control  text-secondary " id="gitlinkprod"
                                    name="gitlinkprod" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="sqllink">Sql Update Link </label> </i>
                                <input type="text" class="form-control  text-secondary " id="envlink"
                                    name="envlink" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="envlink">Env Update Link </label> </i>
                                <input type="text" class="form-control   text-secondary" id="envlink"
                                    name="envlink" placeholder="Enter a URL">
                            </div>

                            <div class="col-sm-6 text-primary">
                                <i class="ti-check-box"> <label for="status"> Status</label> </i>
                                <select class="form-control  text-secondary "id="status" name="status">
                                      <option class=" form-control text-blue ">Test Release Request</option>
                                      <option class=" form-control text-blue ">Test Release Done</option>
                                      <option class=" form-control text-blue ">Prod Release Request</option>
                                      <option class=" form-control text-blue ">Prod Release Done</option>
                                </select>
                            </div>

                            <div class="col-sm-6  text-primary">
                                <i class="ti-clip"><label> Attachments</label></i>
                                <label for="formFile" class="form-control text-secondary "> <i class="ti-clip">Add
                                        Attachment</i></label>
                                <input class="form-control " type="file" id="formFile" name="file[]" multiple
                                    style="display: none">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                id="merge-close">Cancel</button>
                            <button type="button" class="btn btn-primary"
                                name="save-merge"id="">Update</button>
                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>
    {{-- MERGE EDIT MODEL END --}}

    {{-- Approve Model Start --}}

    <div class="modal fade none-border" id="approvemerge">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="font-family: serif;">Merge Request Approve</h5>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark" style="border: none; background: none;">
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        <div class="mb-3">
                            <div class="col-sm-6 " style="max-width: 100%;">
                                <i class="ti-check-box"> <label for="rejectreson">Quality Score</label> </i>
                                 <input type="number" name="qscore" id="qscore" class="form-control  text-secondary " placeholder="Enter Quality Score">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success">Approve</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Approve Model End --}}

    {{-- Merge Reject Start --}}
    <div class="modal fade none-border" id="rejectmerge">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="font-family: serif;">Merge Request Reject</h5>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark" style="border: none; background: none;">
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        <div class="mb-3">
                            <div class="col-sm-6 " style="max-width: 100%;">
                                <i class="ti-check-box"> <label for="rejectreson"> Reject Reason</label> </i>
                                <select class="form-control  text-secondary "id="status" name="status">
                                    <option class=" form-control text-blue ">Merge Conflict</option>
                                    <option class=" form-control text-blue ">Code Error</option>
                                </select>
                            </div>
                            <div class="col-sm-6 " style="max-width: 100%;">
                                <i class="ti-comment-alt"> <label for="rejectcomment"> Reject Comment</label> </i>
                                <textarea class="form-control text-secondary" id="rejectcomment" rows="6" style="height:75px" placeholder="Enter Reject comment"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger">Reject</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Merge Reject End --}}

    <script src="{{ asset('assets/js/lib/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/jquery.nanoscroller.min.js') }}"></script>
    <!-- nano scroller -->
    <script src="{{ asset('assets/js/lib/menubar/sidebar.js') }}"></script>
    <script src="{{ asset('assets/js/lib/preloader/pace.min.js') }}"></script>
    <!-- sidebar -->

    <script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <!-- bootstrap -->

    <script src="{{ asset('assets/js/lib/calendar-2/moment.latest.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.init.js') }}"></script>


    <script src="{{ asset('assets/js/lib/weather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/weather/weather-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/chartist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/sparkline.init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('assets/js/dashboard2.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/admin/merge.js') }}"></script>
    <script src="{{ asset('assets/js/lib/toastr/toastr.min.js') }}"></script>
    @if (session('message'))
        {!! session('message') !!}
    @endif
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('change', "#depstatus_edit", function() {
                var value = $(this).val();
                if (value == "3") {
                    $("#reject_comment").show();
                } else {
                    $("#reject_comment").hide();
                }
            });
        });
    </script>
</body>

</html>
