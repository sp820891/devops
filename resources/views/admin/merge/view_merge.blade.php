@extends('admin.layouts.admin')
@section('content')
     <style>
        .control-label {
            color: #3a2f6bea !important;
        }
     </style>
    <div class="card-body">

        <div class="row comment_p">
            <input type="hidden" id="merge_id" value="{{ $merge->id }}">
            <div class="col-md-11">
                <a href="{{ route('admin.mergerequest') }}" style="cursor: pointer; font-size:18px;"><i
                        class="fa-solid fa-backward"></i></a>
            </div>
            <div>
                <span style="font-size:30px; cursor:pointer; margin-left: 32px;" onclick="openNav()"><i class="fa-solid fa-ellipsis"></i> </span>
            </div>

            <div class="col-md-6">
                <label class="control-label">Sprint</label>
                <p class="text-secondary">{{ $sprint->name }}
                    ({{ \Carbon\Carbon::parse($sprint->start_date)->isoFormat('MMM  D') }} -
                    {{ \Carbon\Carbon::parse($sprint->end_date)->isoFormat('MMM  D') }})</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Project</label>
                <p class="text-secondary">{{ $project->name }}</p>

            </div>
            <div class="col-md-6">
                <label class="control-label">Git Test Link</label>
                <p><a href="{{ $merge->git_test_link }}">{{ $merge->git_test_link }}</a></p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Git Prod Link</label>
                @if ($merge->git_prod_link != NULL)
                  <p><a href="{{ $merge->git_prod_link }}">{{ $merge->git_prod_link }}</a></p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">SQL Update Link</label>
                @if ($merge->sql_update_link != NULL)
                <p><a href="{{ $merge->sql_update_link }}">{{ $merge->sql_update_link }}</a></p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Env Update Link</label>
                @if ($merge->env_update_link != NULL)
                <p><a href="{{ $merge->env_update_link }}">{{ $merge->env_update_link }}</a></p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Priority</label>
                @if ($merge->priority_id  != NULL)
                <p class="text-secondary">{{ $priority->name }}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Release Env</label>
                <p class="text-secondary">{{ $env->name }}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Quality Score</label>
                @if ($merge->quality_score  != NULL)
                <p class="text-secondary">{{ $merge->quality_score }}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Artisan Command</label>
                @if ($merge->artisan_command  != NULL)
                <p class="text-secondary">{{ $merge->artisan_command }}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Issue Url Link</label>
                <p><a href="{{ $merge->issue_url }}">{{ $merge->issue_url }}</a></p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Reject Count</label>
                @if ($merge->reject_count  != NULL)
                <p class="text-secondary">{{$merge->reject_count}}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Test Release Date</label>
                @if ($merge->test_release_count  != NULL)
                <p class="text-secondary">{{$merge->test_release_count}}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Production Release Date</label>
                @if ($merge->prod_release_count  != NULL)
                <p class="text-secondary">{{$merge->prod_release_count}}</p>
                @else
                   <p class="text-secondary">Nil</p>
                @endif
            </div>
            <div class="col-md-6">
                <label class="control-label">Status</label>
                <p class="text-secondary">{{ $status->name }}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Attachments</label>
                <p class="text-secondary">Nil</p>
            </div>
            <div class="col-md-6 reject-commend" id="hidden">
                <label class="control-label ">Reject Comment</label>
                <p class="text-secondary">Nil</p>
                {{-- <textarea class="form-control form-white" placeholder="Enter Command" type="text " id="reject_comment" name="artisan">Nil</textarea> --}}
            </div>
        </div>
        </form>
    </div>
    </div>
    <br>
    </div>
    </div>

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <form class="comment-text">
            <textarea class="form-control form-white comment-box" placeholder="Enter Commend" type="text " name=""
                id="casual_comment"></textarea>
            <button type="submit" class="comment-snd" id="up-comment" style="color: rgb(10, 14, 117);"><i class="fa-solid fa-paper-plane"></i></button>
        </form>
        <div class="m-3" id="comments_view">
        </div>
    </div>
    <script src="{{ asset('assets/js/admin/merge.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "300px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>
@endsection
