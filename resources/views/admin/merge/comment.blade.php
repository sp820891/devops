@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Comments
@endsection
@section('content')
<style>
.comment-textarea{
    width: 225px;
    height: 32px;
    background: white;
    border-radius: 10px;
}
.btn{
    margin-left: 178px;
    margin-top: -56px;
    background: inherit;
}
.list{
  margin-left: 37px;
  margin-right: 45px;
  word-break: break-all;
  margin-top: 28px;
}
</style>

<div id="mySidebar" class="comment">

  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>

<div class="container">

  <form action="" method="post">
    <textarea class="form-control comment-textarea textarea1" placeholder="Enter Your Comment" required> </textarea>
  </form>
</div>
 <ul class="list">
  <li>Bootstrap 5 (released 2021) is the newest version of Bootstrap (released 2013); with new components, faster stylesheet and more responsiveness.
    Bootstrap 5 supports the latest, stable releases of all major browsers and platforms. However, Internet Explorer 11 and down is not supported.
    The main differences between Bootstrap 5 and Bootstrap 3 & 4, is that Bootstrap 5 has switched to vanilla JavaScript instead of jQuery.</li>
 </ul>
 <br>
 </div>
<div class="dropdown">
    <a class="ti-more-alt" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   </a>
<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
<a href="#" class="dropdown-item" onclick="openNav()" type="button"><i class="ti-comment-alt"></i>&nbsp;Comment</a>
<a href="#" class="dropdown-item" type="button"><i class="ti-trash"></i>&nbsp; Delete</button>
    </div>
   </div>
  <h2>Collapsed Sidebar</h2>

  <p>Click on the hamburger menu/bar icon to open the sidebar, and push this content to the right.</p>
 <script>
 function openNav() {
  document.getElementById("mySidebar").style.width = "290px";
 }
 function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
 }
 </script>
<script src="{{ asset('assets/js/comment.js') }}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.6.3.min.js"></script>
@endsection
