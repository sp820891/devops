@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Release Logs
@endsection
@section('content')
<div class="card-body">
    <div class="row">
        <h4 class="heading_h4">Release Logs</h4>
    </div><br><br>
    <!-- end col -->

</div>
<br>
<input type="hidden" id="id_delete_log">
<table  width="100%" class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Issue Url</th>
            <th scope="col">Git Test Link</th>
            <th scope="col">Rejected By</th>
            @can('can delete release logs')
            <th scope="col">Action</th>
            @endcan
        </tr>
    </thead>
    <tbody id="releaselog-table">
    </tbody>
</table>
</div>
</div>
{{-- DELETE MODEL START --}}
<div class="modal fade none-border" id="delete-log">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_log">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Release Log <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deleteLog()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- DELETE MODEL END --}}
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset('assets/js/admin/api/releaselog.js')}}"></script>
@endsection

