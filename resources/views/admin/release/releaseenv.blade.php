@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Release Env
@endsection
@section('content')
<div class="card-body">
    <div class="row">
        <h4 class="heading_h4">Release Env</h4>
        <div class="offset-lg-10 col-lg-2">
            @can('can create release env')

            <a href="#" data-toggle="modal" data-target="#add-env"
                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                float: right">
                <i class="fa fa-plus"></i>
            </a>
            @else
            <a href="#"  data-toggle="modal" data-target="#add-env"
            class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px; visibility: hidden;
            float: right">
            <i class="fa fa-plus"></i>
        </a>
            @endcan
        </div>
    </div>
    <!-- end col -->
    <!-- BEGIN MODAL -->
    <!-- Modal Add Env-->
    <div class="modal fade none-border" id="add-env">
        <div class="modal-dialog">
            <div class="modal-content user-modal">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true" id="close"><i
                        class="fa-sharp fa-solid fa-xmark"></i></button>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Release Env</label>
                                <input class="form-control form-white" placeholder="Enter Name" type="text"
                                    name="name" id="name" />
                                <small id="small_env_name" class="text-danger"></small>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Description</label>
                                <textarea name="description" class="form-control form-white" id="description" cols="30" rows="3"></textarea>
                                <small id="small_env_des" class="text-danger"></small>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="mb-2 submit-btn">
                    <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                     id="save-releaseenv">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL -->
</div>
<br>
<table class="table">
    <thead class="thead-light">

        <tr>
            <th scope="col">Idf</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Created By</th>
            @can( "can delete release env")
            <th scope="col">Action</th>
            @endcan
        </tr>
    </thead>
    <tbody id="releaseenv-table">
    </tbody>
</table>
</div>
</div>
{{-- DELETE MODEL START --}}
<div class="modal fade none-border" id="delete-env">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_env">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Release Env <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deleteEnv()" id="delete_btn">Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- DELETE MODEL END --}}
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset("assets/js/admin/api/releaseenv.js")}}"></script>
@endsection
