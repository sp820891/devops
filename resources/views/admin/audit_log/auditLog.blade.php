<div class="col-lg-12">
    <div class="card">
        <div class="card-title pr">
            <h6>Audit Log</h6>
        </div>
        <div class="card-body" style="height:10em; overflow-y: auto;">
            <div class="table-responsive">
                <table class="table student-data-table m-t-20 w-100" >
                    <tbody>
                        @foreach ($log as $logs)
                            <tr>
                                <td style='  text-align: left;'><i style='font-size:15px' class='fas'>&#xf044;</i>
                                    {{ $logs->event }} {{ substr($logs->auditable_type, 11) }} ID
                                    -{{ $logs->auditable_id }}
                                    @if ($logs->event == 'updated')
                                        from <b>{{ str_replace(['{', '"', '}'], ' ', $logs->old_values) }} </b> to
                                        <b>{{ str_replace(['{', '"', '}'], ' ', $logs->new_values) }}</b>
                                    @endif
                                    at {{ $logs->created_at }} by {{ $logs->user['email'] }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
