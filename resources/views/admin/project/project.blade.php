@extends('admin.layouts.admin')
@section('title')
{{auth()->user()->roles->pluck('name')->first()}} . Projects
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <h4 class="heading_h4">Projects</h4>
            <div class="offset-lg-10 col-lg-2">
                @can('can create a project')
                <a href="#" data-toggle="modal" data-target="#add-project"
                                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px;
                                float: right">
                                <i class="fa fa-plus"></i>
                            </a>
                            @else
                            <a href="#"  data-toggle="modal" data-target="#add-project"
                                class="btn btn-sm badge-primary btn-block waves-effect waves-light" style="width: 35px; visibility: hidden;
                                float: right">
                                <i class="fa fa-plus"></i>
                            </a>
                @endcan
            </div>
        </div>
        <!-- end col -->
        <!-- BEGIN MODAL -->
        <!-- Modal Add Project -->
        <div class="modal fade none-border" id="add-project">
            <div class="modal-dialog">
                <div class="modal-content user-modal">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true"><i
                            class="fa-sharp fa-solid fa-xmark" id="close"></i></button>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Project</label>
                                    <input class="form-control form-white" placeholder="Enter Project Name" type="text"
                                        name="name" id="project_name" />
                                    <small id="small_project_name" class="text-danger"></small>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Description</label>
                                    <textarea name="description" class="form-control form-white" id="project_des" cols="30" rows="3"></textarea>
                                    <small id="small_project_des" class="text-danger"></small>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="submit-btn mb-3">
                        <button type="button" class="btn badge-primary waves-effect waves-light save-category"
                        id="save-project">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL -->
    </div>
<br>
<table class="table table-hover">
    <thead class="thead-light">
        <tr class="border-primary">
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Created By</th>
            @can("can delete a project")
              <th scope="col">Action</th>
            @endcan
        </tr>
    </thead>
    <tbody id="project-table">
    </tbody>
</table>
{{-- DELETE MODEL START --}}
<div class="modal fade none-border" id="delete-project">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="row">
                        <input type="hidden" id="id_delete_project">
                        <div class="col-md-12">
                            <h5> Are you sure want to delete Project <span id="user_name" class="text-danger"></span>?</i></h5>
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex mb-2 submit_delete_btn" style="margin-left: 67%;">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" id="delete_btn" class="btn btn-danger waves-effect waves-light save-category"
                onclick="deleteProject()">Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- DELETE MODEL END --}}


</div>
</div>
<!----------------------------------AuditLog------------------------------->
@include('admin.audit_log.auditLog')
<script src="{{asset('assets/js/admin/api/project.js')}}"></script>
@endsection
