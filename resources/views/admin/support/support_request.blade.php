<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- theme meta -->
    <meta name="theme-name" content="focus" />
    <title>{{ auth()->user()->roles->pluck('name')->first() }} . Support Requests</title>
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
    <!-- Styles -->
    <link href="{{ asset('assets/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/weather-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/modelstyle.css') }}">
    <script src="{{ asset('assets/js/sweetalert2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/toastr/toastr.min.css') }}">
    <!--font cdn-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
</head>

<body>
    @include('admin.layouts.sidebar')
    <!-- /# sidebar -->
    @include('admin.layouts.navbar')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="p-2 d-flex">
                                    <div>
                                        @can('can create support requests')
                                            <a href="#" data-toggle="modal" data-target="#addsupport"
                                                class="btn  btn-sm badge-primary btn-block waves-effect waves-light"
                                                style="width: 100px;">
                                                <i class="ti-plus"></i> New
                                            </a>
                                        @endcan
                                    </div>
                                    <div class="pl-3">
                                        @can(['can view support requests', 'can edit support requests', 'can delete
                                            support requests'])
                                            <div class="dropdown">
                                                <button
                                                    style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa-solid fa-ellipsis" title="Options"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" id="view_supportrequests"
                                                        style="cursor:pointer;">View</a>
                                                    <a class="dropdown-item" data-toggle="modal" id="edit-support"
                                                        style="cursor:pointer;"> Edit</a>
                                                    <a class="dropdown-item" id="delete-support"> Delete </a>
                                                </div>
                                            </div>
                                        </div>
                                    @elsecan(['can view support requests','can edit support requests'])
                                        <div class="dropdown">
                                            <button
                                                style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis" title="Options"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" id="view_supportrequests"
                                                    style="cursor:pointer;">View</a>
                                                <a class="dropdown-item" data-toggle="modal" id="edit-support"
                                                    style="cursor:pointer;"> Edit</a>
                                            </div>
                                        </div>
                                    @elsecan(['can edit support requests','can delete support requests'])
                                        <div class="dropdown">
                                            <button
                                                style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis" title="Options"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" data-toggle="modal" id="edit-support"
                                                    style="cursor:pointer;"> Edit</a>
                                                <a class="dropdown-item" id="delete-support"> Delete </a>
                                            </div>
                                        </div>
                                    @elsecan(['can view support requests','can delete support requests'])
                                        <div class="dropdown">
                                            <button
                                                style="background-color: white; border:none; cursor:pointer; font-size:22px;"
                                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis" title="Options"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" id="view_supportrequests"
                                                    style="cursor:pointer;">View</a>
                                                <a class="dropdown-item" id="delete-support"> Delete </a>
                                            </div>
                                        </div>
                                    @elsecan('can view support requests')
                                        <a id="view_supportrequests" class="btn  btn-sm btn-warning text-light"
                                            style="width: 100px;">
                                            <i class="ti-eye"></i> View
                                        </a>
                                    @elsecan('can edit support requests')
                                        <a data-toggle="modal" id="edit-support"
                                            class="btn  btn-sm btn-success text-light" style="width: 100px;">
                                            <i class="ti-pencil-alt"></i> Edit
                                        </a>
                                    @elsecan('can delete support requests')
                                        <a id="delete-support" class="btn  btn-sm btn-danger text-light"
                                            style="width: 100px;">
                                            <i class="ti-trash"></i> Delete
                                        </a>
                                    @endcan
                                </div>


                                <div class="heading_h5">
                                    <h3>Support Requests</h3>
                                </div>
                            </div>

                            <br>

                            <table class="table table-hover" style="width:100%;">
                                <thead style="background: rgb(231, 228, 228)">
                                    <tr>
                                        <th><input type="checkbox" id="selectid"></th>
                                        <th>Notes</th>
                                        <th>Status</th>
                                        <th>Project</th>
                                        <th>Release_Env</th>
                                        <th>Issue Url Link</th>
                                        <th>Priority</th>
                                    </tr>
                                </thead>
                                <tbody id="merge-table" style="width:100%; ">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!----------------------------------AuditLog------------------------------->
                    @include('admin.audit_log.auditLog')
            </div>
        </div>

    </div>

    </section>
    </div>
    </div>
    </div>


    {{-- INSERT MODEL START --}}
    <div class="modal fade none-border" id="addsupport">
        <div class="modal-dialog">
            <div class="modal-content merge-model">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Support Request</h5>
                    <button type="button" class="ti-close" data-dismiss="modal"
                        aria-label="fa-sharp fa-solid fa-xmark"
                        style="border: none;
                background: none;">
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" name="support_send" id="support_send">
                        <div class="mb-3 row">
                            <div class="col-sm-6 text-primary">
                                <i class="ti-link"> <label for="issueurl">Issue Url </label> </i>
                                <input type="text" class="form-control text-secondary" id="issueurl"
                                    name="issueurl">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-notepad"> <label for="supportnotes">Notes </label> </i>
                                <input type="text" class="form-control text-secondary" id="supportnotes"
                                    name="supportnotes">
                            </div>
                            <div class="col-sm-6 text-primary">
                                <i class="ti-check-box"> <label for="supportpriority"> Priority</label> </i>
                                <select class="form-control  text-secondary "id="supportpriority" name="supportpriority">
                                    <option>Medium </option>

                                </select>
                            </div>
                            <div class="col-sm-6  text-primary">
                                <i class="ti-clip"><label> Attachments</label></i>
                                <label for="formFile" class="form-control text-secondary "> <i class="ti-clip">Add
                                        Attachment</i></label>
                                <input class="form-control " type="file" id="formFile" name="file[]" multiple
                                    style="display: none">
                            </div>
                        </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                    id="merge-close">Close</button>
                                <button type="button" class="btn btn-primary" name="save-merge"
                                    id="">Save</button>
                            </div>
                        </form>

                        </div>
                </div>
            </div>
        </div>
    </div>
    {{-- INSERT MODEL END --}}


    <script src="{{ asset('assets/js/lib/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/jquery.nanoscroller.min.js') }}"></script>
    <!-- nano scroller -->
    <script src="{{ asset('assets/js/lib/menubar/sidebar.js') }}"></script>
    <script src="{{ asset('assets/js/lib/preloader/pace.min.js') }}"></script>
    <!-- sidebar -->

    <script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <!-- bootstrap -->

    <script src="{{ asset('assets/js/lib/calendar-2/moment.latest.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/calendar-2/pignose.init.js') }}"></script>


    <script src="{{ asset('assets/js/lib/weather/jquery.simpleWeather.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/weather/weather-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/circle-progress/circle-progress-init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/chartist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/sparklinechart/sparkline.init.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('assets/js/dashboard2.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/admin/support.js') }}"></script>
    <script src="{{ asset('assets/js/lib/toastr/toastr.min.js') }}"></script>
    @if (session('message'))
        {!! session('message') !!}
    @endif
</body>

</html>
