@extends('admin.layouts.admin')
@section('content')
<style>
    .control-label {
            color: #3a2f6bea !important;
        }
</style>
    <div class="card-body">
        <div class="row comment_p">
            {{-- <input type="hidden" id="support_id" value="{{ $support->id }}"> --}}
            <div class="col-md-11">
                <a href="{{ route('admin.supportrequest') }}" style="cursor: pointer; font-size:18px;"><i
                        class="fa-solid fa-backward"></i></a>
            </div>

            <div class="col-md-6">
                <label class="control-label">Project</label>
                <p class="text-secondary">{{ $supportrequest->notes}}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Priority</label>
                <p class="text-secondary">{{ $supportrequest->priority->name }}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Release Env</label>
                <p class="text-secondary">{{ $supportrequest->Releaseenv->name }}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Issue Url Link</label>
                <p><a href="{{ $supportrequest->issue_url }}">{{ $supportrequest->issue_url }}</a></p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Status</label>
                <p class="text-secondary">{{ $supportrequest->status->name }}</p>
            </div>
            <div class="col-md-6">
                <label class="control-label">Attachments</label>
                <p class="text-secondary">Nil</p>
            </div>
        </div>
        </form>
    </div>
    </div>
    <br>
    </div>
    </div>
    <script src="{{ asset('assets/js/admin/support.js') }}"></script>
@endsection
