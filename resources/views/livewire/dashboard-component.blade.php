<div>
    @include('admin.layouts.sidebar')
    <!-- /# sidebar -->

    @include('admin.layouts.navbar')


    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h4 class="heading_h4">Dashboard</h4>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->

                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-3">
                              <span wire:click='merge_count'>
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-link color-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Merge Requests</div>
                                        <div class="stat-digit text-center">{{$merge->count()}}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                        <div class="col-lg-3">
                              <a wire:click='reject_count'>
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-close color-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Reject Count</div>
                                        <div class="stat-digit text-center">{{$reject_count}}</div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                              <a wire:click='test_count'>
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-link color-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Test Release</div>
                                        <div class="stat-digit text-center">{{$test_count}}</div>
                                        {{$switch}}
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3">
                              <a wire:click='prod_count'>
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-location-arrow color-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Production Release</div>
                                        <div class="stat-digit text-center">{{$prod_count}}</div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-12">
                            <div class="card">
                                @if($switch == 1)
                                   @php
                                        $reject->render();
                                   @endphp
                                {{-- if($switch == 2){

                                $reject->render();

                                } if($switch == 3){

                                $sprint->render();

                                }
                                if($switch == 4){

                                $env->render();

                                } --}}
                            @endif
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
            </section>
        </div>
    </div>
    </div>
</div>
