<section style="margin-left: 20px;">
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Profile Information') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
            {{ __("Update your account's profile information and email address.") }}
        </p>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('profile.update') }}" class="mt-6 space-y-6" enctype="multipart/form-data">
        @csrf
        @method('patch')
            <div>
                <x-input-label :value="__('Profile Picture')" />
                <span class="hoverable_profile"><i class="fas fa-camera"></i></span>
                <span class="hoverable_profile_text">Change Profile</span>
                @if (auth()->user()->profile_picture == NULL)
                  <img src="{{asset('profile_picture/blank.png')}}" id="profile_select" alt="profile_picture" width="100px" style="cursor: pointer; border-radius:50px; height:100px;">
                @else
                  <img src="{{asset(auth()->user()->profile_picture)}}" id="profile_select" alt="profile_picture" width="100px" style="cursor: pointer; border-radius:50px; height:100px;">
                @endif
                <input type="file" name="profile_picture" id="profile_upload" style="display: none;">
            </div>
            <div>
                <x-input-label for="name" :value="__('First Name')" />
                <x-text-input id="name" name="first_name" type="text" class="mt-1 block w-full" :value="old('first_name', $user->first_name)" required autofocus autocomplete="name" />
                <x-input-error class="mt-2" :messages="$errors->get('first_name')" />
            </div>
            <div>
                <x-input-label for="name" :value="__('Last Name')" />
                <x-text-input id="name" name="last_name" type="text" class="mt-1 block w-full" :value="old('last_name', $user->last_name)" required autofocus autocomplete="name" />
                <x-input-error class="mt-2" :messages="$errors->get('last_name')" />
            </div>
            <div>
                <x-input-label for="name" :value="__('Gender')" />
                <x-text-input id="name" type="text" class="mt-1 block w-full" :value="old('gender', $user->gender)" readonly autofocus autocomplete="name" />
            </div>
            <div>
                <x-input-label for="email" :value="__('Email')" />
                <x-text-input id="email" name="email" type="email" class="mt-1 block w-full" :value="old('email', $user->email)" readonly required autocomplete="username" />
                <x-input-error class="mt-2" :messages="$errors->get('email')" />
        </div>

            @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && ! $user->hasVerifiedEmail())
                <div>
                    <p class="text-sm mt-2 text-gray-800 dark:text-gray-200">
                        {{ __('Your email address is unverified.') }}

                        <button form="send-verification" class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800">
                            {{ __('Click here to re-send the verification email.') }}
                        </button>
                    </p>

                    @if (session('status') === 'verification-link-sent')
                        <p class="mt-2 font-medium text-sm text-green-600 dark:text-green-400">
                            {{ __('A new verification link has been sent to your email address.') }}
                        </p>
                    @endif
                </div>
            @endif

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>
        </div>
    </form>
</section>
