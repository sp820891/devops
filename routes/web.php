<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MergReportController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\MergerequestController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\SupportRequestController;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
require __DIR__.'/auth.php';
Route::middleware('auth')->group(function () {
    Route::group(['middleware' => ['permission:can access dashboard']], function () {
        Route::get('/dashboard',[AdminController::class,'dashboard'])->name('admin.dashboard');
        //Koolreport For Dashboard
        Route::get('/merg-report/{id}',[MergReportController::class,'merg']);
    });
    Route::group(['middleware' => ['permission:can access merge requests']], function () {
        Route::get('/mergerequests',[AdminController::class,'merge'])->name('admin.mergerequest');
        Route::get('/view_mergerequests/{id}',[MergerequestController::class,'view_merge']);
        Route::get('/fetchMerge',[MergerequestController::class,'show']);
        Route::group(['middleware' => ['permission:can create merge requests']], function () {
            Route::post('/addMerge',[MergerequestController::class,'store']);
        });
        Route::group(['middleware' => ['permission:can edit merge requests']], function () {
            Route::post('/editMerge',[MergerequestController::class,'edit']);
            Route::post('/updateMerge/{id}',[MergerequestController::class,'update']);
        });
        Route::group(['middleware' => ['permission:can delete merge requests']], function () {
            Route::post('/deleteMerge',[MergerequestController::class,'destroy']);
        });
    });
    Route::group(['middleware' => ['permission:can access support requests']], function () {
        Route::get('/supportrequests',[AdminController::class,'support'])->name('admin.supportrequest');
        Route::get('/view_supportrequests/{id}',[SupportRequestController::class,'view_support']);
        Route::get('/fetchsupport',[SupportRequestController::class,'show']);
        Route::get('/suppott',[SupportRequestController::class,'']);
        Route::group(['middleware' => ['permission:can create support requests']], function () {
            Route::post('/addSupport',[SupportRequestController::class,'store']);
        });
        Route::group(['middleware' => ['permission:can edit support requests']], function () {
            Route::post('/editSupport',[SupportRequestController::class,'edit']);
            Route::post('/updateSupport/{id}',[SupportRequestController::class,'update']);
        });
        Route::group(['middleware' => ['permission:can delete support requests']], function () {
           Route::post('/deleteSupport',[SupportRequestController::class,'destroy']);
        });
    });
    Route::group(['middleware' => ['permission:can access a project']], function () {
        Route::get('/projects',[AdminController::class,'project'])->name('admin.project');
    });
    Route::group(['middleware' => ['permission:can access sprint']], function () {
        Route::get('/sprints',[AdminController::class,'sprint'])->name('admin.sprint');
    });
    Route::group(['middleware' => ['permission:can access release env']], function () {
        Route::get('/releaseenv',[AdminController::class,'releaseenv'])->name('admin.releaseenv');
    });
    Route::group(['middleware' => ['permission:can access priority']], function () {
        Route::get('/priority',[AdminController::class,'priority'])->name('admin.priority');
    });
    Route::group(['middleware' => ['permission:can access deployment status']], function () {
        Route::get('/deploymentstatus',[AdminController::class,'deployment'])->name('admin.deploymentstatus');
    });
    Route::group(['middleware' => ['permission:can access release logs']], function () {
        Route::get('/releaselogs',[AdminController::class, 'releaselog'])->name('admin.releaselog');
    });
    Route::group(['middleware' => ['permission:can access quality status']], function () {
        Route::get('/qualitystatus',[AdminController::class, 'qualitystatus'])->name('admin.qualitystatus');
    });
    Route::group(['middleware' => ['permission:can access comment']], function () {
        Route::get('/comment',[AdminController::class,'comment'])->name('admin.comment');
        Route::post('/updateComment/{id}',[MergerequestController::class,'updateComment']);
        Route::get('/showComment/{id}',[MergerequestController::class,'showComment']);
    });
    Route::group(['middleware' => ['permission:can access documents from merge request']], function () {
        Route::get('/documents',[AdminController::class,'documents'])->name('admin.documents');
    });
    Route::group(['middleware' => ['permission:can access status']], function () {
        Route::get( '/status',[AdminController::class, 'status'])->name('admin.status');
    });
    Route::group(['middleware' => ['permission:can access a roles']], function () {
        Route::get('/roles',[AdminController::class,'role'])->name('admin.role');
        Route::get('/fetchRole',[AdminController::class,'fetchRole']);
        Route::group(['middleware' => ['permission:can add roles']], function () {
            Route::post('/addRole',[AdminController::class,'addRole']);
        });
        Route::get('/viewRole/{id}',[AdminController::class,'viewRole']);
        Route::group(['middleware' => ['permission:can delete roles']], function () {
            Route::get('/deleteRole/{id}',[AdminController::class, 'deleteRole']);
        });
        Route::group(['middleware' => ['permission:can access role based permissions']], function () {
            Route::get('/roleandpermissions/{id}',[AdminController::class,'rbp'])->name('admin.rbp');
            Route::get('/fetchRoleWithPermission/{role}',[AdminController::class,'fetchRoleWithPermission']);
            Route::post('/assignRoleWithPermission/{role}',[AdminController::class,'assignRoleWithPermission']);
        });
    });
    Route::group(['middleware' => ['permission:can access permissions']], function () {
        Route::get('/permissions',[AdminController::class,'permissions'])->name('admin.permissions');
        Route::get('/fetchPermission',[AdminController::class,'fetchPermission']);
        Route::group(['middleware' => ['permission:can add permissions']], function () {
            Route::post('/addPermission',[AdminController::class,'addPermission']);
        });
        Route::get('/viewPermission/{id}',[AdminController::class,'viewPermission']);
        Route::group(['middleware' => ['permission:can delete permissions']], function () {
            Route::get('/deletePermission/{id}',[AdminController::class, 'deletePermission']);
        });
    });
    Route::group(['middleware' => ['permission:can access users']], function () {
        Route::get('/users',[AdminController::class,'user'])->name('admin.users');
        Route::group(['middleware' => ['permission:can add users']], function () {
            Route::post('/addUser',[AdminController::class,'addUser']);
        });
        Route::get('/fetchUser',[AdminController::class,'fetchUser']);
        Route::group(['middleware' => ['permission:can edit users']], function () {
            Route::get('/editUser/{id}',[AdminController::class,'editUser']);
            Route::post('/updateUser/{id}',[AdminController::class,'updateUser']);
        });
        Route::group(['middleware' => ['permission:can delete users']], function () {
            Route::get('/deleteUser/{id}',[AdminController::class, 'deleteUser']);
        });
        Route::group(['middleware' => ['permission:can access user based permissions']], function () {
            Route::get('/userandpermissions/{id}',[AdminController::class,'ubp'])->name('admin.ubp');
            Route::get('/fetchUserWithPermission/{user_id}',[AdminController::class,'fetchUserWithPermission']);
            Route::post('/assignUserWithPermission/{user_id}',[AdminController::class,'assignUserWithPermission']);
        });
    });
    //Email and Notification
    Route::post('userCredentialMailler',[AdminController::class,'userCredentialMailler']);
    Route::get('set-password/{token}/{email}',[AdminController::class,'setPassword'])->name('admin.setPassword');
    Route::post('mailAndNotification',[AdminController::class,'mailAndNotification']);
});


// merg-notify
Route::get('merg-notify',[NotificationController::class, 'merg']);
