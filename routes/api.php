<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\SprintController;
use App\Http\Controllers\Api\StatusController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\PriorityController;
use App\Http\Controllers\Api\ReleaseenvController;
use App\Http\Controllers\Api\QualitystatusController;
use App\Http\Controllers\Api\ReleaselogController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DeploymentstatusController;
use App\Http\Controllers\Api\DocumentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth')->group(function () {
Route::group(['middleware' => ['permission:can access sprint']], function () {
    Route::get( '/sprint',[SprintController::class, 'sprint']);
    Route::group(['middleware' => ['permission:can create sprint']], function () {
        Route::post( '/store-sprint',[SprintController::class, 'storeSprint']);
    });
    Route::group(['middleware' => ['permission:can delete sprint']], function () {
        Route::get( '/fetchSprintId/{id}',[SprintController::class, 'fetchSprintId']);
        Route::delete('delete-sprint/{id}/delete',[SprintController::class, 'deleteSprint']);
    });
});
Route::group(['middleware' => ['permission:can access status']], function () {
    Route::get( '/status',[StatusController::class, 'status']);
    Route::group(['middleware' => ['permission:can create status']], function () {
        Route::post( '/store-status',[StatusController::class, 'storeStatus']);
    });
    Route::group(['middleware' => ['permission:can delete status']], function () {
        Route::get( '/fetchStatusId/{id}',[StatusController::class, 'fetchStatusId']);
        Route::delete('delete-status/{id}/delete',[StatusController::class, 'deleteStatus']);
    });
});
Route::group(['middleware' => ['permission:can access a project']], function () {
    Route::get( '/project',[ProjectController::class, 'project']);
    Route::group(['middleware' => ['permission:can create a project']], function () {
        Route::post( '/store-project',[ProjectController::class, 'storeProject']);
    });
    Route::group(['middleware' => ['permission:can delete a project']], function () {
        Route::get( '/fetchProjectId/{id}',[ProjectController::class, 'fetchProjectId']);
        Route::delete('delete-project/{id}',[ProjectController::class, 'deleteProject']);
    });
});
Route::group(['middleware' => ['permission:can access priority']], function () {
    Route::get( '/priority',[PriorityController::class, 'priority']);
    Route::group(['middleware' => ['permission:can create priority']], function () {
        Route::post( '/store-priority',[PriorityController::class, 'storePriority']);
    });
    Route::group(['middleware' => ['permission:can delete priority']], function () {
        Route::get( '/fetchPriorityId/{id}',[PriorityController::class, 'fetchPriorityId']);
        Route::delete('delete-priority/{id}/delete',[PriorityController::class, 'deletePriority']);
    });
});
Route::group(['middleware' => ['permission:can access release env']], function () {
    Route::get( '/releaseenv',[ReleaseenvController::class, 'releaseenv']);
    Route::group(['middleware' => ['permission:can create release env']], function () {
        Route::post( '/store-releaseenv',[ReleaseenvController::class, 'storeReleaseenv']);
    });
    Route::group(['middleware' => ['permission:can delete release env']], function () {
        Route::get( '/fetchEnvId/{id}',[ReleaseenvController::class, 'fetchEnvId']);
        Route::delete('delete-releaseenv/{id}/delete',[ReleaseenvController::class, 'deleteReleaseenv']);
    });
});
Route::group(['middleware' => ['permission:can access quality status']], function () {
    Route::get( '/qualitystatus',[QualitystatusController::class, 'qualitystatus']);
    Route::group(['middleware' => ['permission:can create quality status']], function () {
        Route::post( '/store-qualitystatus',[QualitystatusController::class, 'storeQualitystatus']);
    });
    Route::group(['middleware' => ['permission:can delete quality status']], function () {
        Route::get( '/fetchQtyStatusId/{id}',[QualitystatusController::class, 'fetchQtyStatusId']);
        Route::delete('delete-qualitystatus/{id}/delete',[QualitystatusController::class, 'deleteQualitystatus']);
    });
});
Route::group(['middleware' => ['permission:can access release logs']], function () {
    Route::get( '/releaselog',[ReleaselogController::class, 'releaselog']);
    Route::group(['middleware' => ['permission:can delete release logs']], function () {
        Route::get( '/fetchLogsId/{id}',[ReleaselogController::class, 'fetchLogsId']);
        Route::delete('delete-releaselog/{id}/delete',[ReleaselogController::class, 'deleteReleaselog']);
    });
});
Route::group(['middleware' => ['permission:can access deployment status']], function () {
    Route::get( '/deploymentstatus',[DeploymentstatusController::class, 'deploymentstatus']);
    Route::group(['middleware' => ['permission:can create deployment status']], function () {
        Route::post( '/store-deploymentstatus',[DeploymentstatusController::class, 'storeDeploymentstatus']);
    });
    Route::group(['middleware' => ['permission:can delete deployment status']], function () {
        Route::get( '/fetchDepStatusId/{id}',[DeploymentstatusController::class, 'fetchDepStatusId']);
        Route::delete('delete-deploymentstatus/{id}/delete',[DeploymentstatusController::class, 'deleteDeploymentstatus']);
    });

});
Route::group(['middleware' => ['permission:can delete documents from merge request']], function () {
    Route::delete('delete-document/{id}/delete',[DocumentController::class, 'deleteDocument']);
});
});
