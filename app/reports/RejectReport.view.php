<?php

use koolreport\processes\ColumnMeta;
use \koolreport\widgets\koolphp\Table;
use koolreport\widgets\google\PieChart as GooglePieChart;
use koolreport\widgets\google\BarChart;
?>
<html>
    <head>
    <title>My Report</title>
    </head>
    <body>

        <?php
        Table::create([
            "dataSource"=>$this->dataStore("mergrequests"),
            "columns"=>array(
                'id',
                'project_id',
                'user_id',
                'quality_score',

            )
        ]);
        ?>
    </body>
</html>
