<?php
namespace App\Reports;
class MyReport extends \koolreport\KoolReport
{
    use \koolreport\laravel\Friendship;
    // By adding above statement, you have claim the friendship between two frameworks
    // As a result, this report will be able to accessed all databases of Laravel
    // There are no need to define the settings() function anymore
    // while you can do so if you have other datasources rather than those
    // defined in Laravel.
    function setup () {
        // Let say, you have "sale_database" is defined in Laravel's database settings.
        // Now you can use that database without any futher setitngs.


        $this->src("mysql") // use any of your preferred connection type in config/database.php
        ->query("SELECT * FROM mergrequests INNER JOIN users ON users.id = mergrequests.user_id")
        // ->query("SELECT * COUNT(user_id) FROM mergrequests GROUP BY user_id ORDER BY COUNT(user_id) DESC;")
        ->pipe($this->dataStore("mergrequests"));
    }
}
