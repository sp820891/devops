<?php

namespace App\Http\Livewire;

use App\Models\Notification as ModelsNotification;
use Livewire\Component;

class Notification extends Component
{
    public function render()
    {
        return view('livewire.notification',[
            'count' => ModelsNotification::where('is_checked','=','0')->count()
        ]);
    }
}
