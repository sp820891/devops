<?php

namespace App\Http\Livewire;

use App\Models\Mergrequest;
use App\Models\Releaselog;
use App\Reports\EnvReport;
use App\Reports\MyReport;
use App\Reports\RejectReport;
use App\Reports\SprintReport;
use Livewire\Component;

class DashboardComponent extends Component
{
    public $switch=0,$report,$reject,$env;
    public function merge_count()
    {
        $this->switch = 1;
        $this->report = new MyReport();
    }
    public function reject_count()
    {
        $this->reject = new RejectReport();
        $this->reject->run();
        $this->switch = 2;
    }
    public function test_count()
    {
        // $sprint = new SprintReport();
        // $sprint->run();
        $this->switch = 3;
    }
    public function prod_count()
    {
        // $this->env = new EnvReport();
        // $this->env->run();
        $this->switch = 4;
    }
    public function render()
    {
        return view('livewire.dashboard-component',[
            'merge' => Mergrequest::all(),
            'reject_count' => Releaselog::count(),
            'test_count' => Mergrequest::sum('test_release_count'),
            'prod_count' => Mergrequest::sum('prod_release_count'),
        ]);
    }
}
