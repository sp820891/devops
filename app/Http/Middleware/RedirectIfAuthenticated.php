<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $user = User::find(auth()->user()->id);
                if ($user->hasPermissionTo('can access dashboard')) {
                    return redirect()->intended(RouteServiceProvider::HOME);
                }
                else if ($user->hasPermissionTo('can access merge requests')) {
                    return redirect()->intended(RouteServiceProvider::MERGE);
                }
                else if ($user->hasPermissionTo('can access support requests')) {
                    return redirect()->intended(RouteServiceProvider::SUPPORT);
                }
                else if ($user->hasPermissionTo('can access a project')) {
                    return redirect()->intended(RouteServiceProvider::PROJECT);
                }
                else if ($user->hasPermissionTo('can access sprint')) {
                    return redirect()->intended(RouteServiceProvider::SPRINT);
                }
                else if ($user->hasPermissionTo('can create release env')) {
                    return redirect()->intended(RouteServiceProvider::RELEASEENV);
                }
                else if ($user->hasPermissionTo('can access priority')) {
                    return redirect()->intended(RouteServiceProvider::PRIORITY);
                }
                else if ($user->hasPermissionTo('can access deployment status')) {
                    return redirect()->intended(RouteServiceProvider::DEPSTATUS);
                }
                else if ($user->hasPermissionTo('can access quality status')) {
                    return redirect()->intended(RouteServiceProvider::QTYSTATUS);
                }
                else if ($user->hasPermissionTo('can access release logs')) {
                    return redirect()->intended(RouteServiceProvider::RELEASELOG);
                }
                else if ($user->hasPermissionTo('can access status')) {
                    return redirect()->intended(RouteServiceProvider::STATUS);
                }
                else if ($user->hasPermissionTo('can access users')) {
                    return redirect()->intended(RouteServiceProvider::USERS);
                }
                else if ($user->hasPermissionTo('can access permissions')) {
                    return redirect()->intended(RouteServiceProvider::PERMISSION);
                }
                else if ($user->hasPermissionTo('can access a roles')) {
                    return redirect()->intended(RouteServiceProvider::ROLE);
                }
                else {
                    Auth::logout();
                    return redirect("/")->withMessage('<script>toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }
                    toastr.error("You Have No Permissions To Access DevOps Portal");</script>');
                }
            }
        }

        return $next($request);
    }
}
