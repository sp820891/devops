<?php

namespace App\Http\Controllers;
use App\Models\Support;
use App\Models\supportDoucument;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Status;
use App\Models\Releaseenv;
use App\Models\priority;
use Illuminate\Support\Facades\Auth;

class SupportRequestController extends Controller
{

  public function store(Request $request ){
    if($request->file){
    $support = new Support();
    $support->notes=$request->input('notes-support');
    $support->status_id=$request->input('status-support');
    $support->project_id=$request->input('project-support');
    $support->releaseenv_id=$request->input('releaseenv-support');
    $support->issue_url=$request->input('issueurl-support');
    $support->priority_id=$request->input('priority-support');
    $support->save();
    if($request->file) {
        foreach ($request->file as $key => $value) {
         $document = new supportDoucument();
         $fileName = time() . '-' . $value->getClientOriginalName();
         $value->move(public_path("support-uploads"), $fileName);
         $document->Filepath = $fileName;
         $document->supportrequest_id = $support->id;
         $document->created_by=Auth::user()->id;
         $document->save();
        }
     }

      return response()->json(['status'=>200 ,'message'=>'request']);
    }
    else{
        $support = new Support();
        $support->notes=$request->input('notes-support');
        $support->status_id=$request->input('status-support');
        $support->project_id=$request->input('project-support');
        $support->releaseenv_id=$request->input('releaseenv-support');
        $support->issue_url=$request->input('issueurl-support');
        $support->priority_id=$request->input('priority-support');
        $support->save();
        return response()->json(['status'=>200 ,'message'=>'successfully']);
    }
  }
  public function show(){
    $supportrequest=Support::with('project','status','releaseenv','priority')->get();
                return response()->json([
                    'status'=>200,
                    'support'=>$supportrequest
                ]);
   }
   public function destroy(Request $request){
    foreach ($request->support as $key => $value) {
        // print_r($value);
        Support::find($value)->delete();
    }
        return response()->json([
        'status' => 200,
        ]);
   }

   public function edit(Request $request)
    {
        $support = Support::find($request->support);


        return response()->json(
            [
                'support' => $support,
                'project' => Project::all(),
                'statu' => Status::all(),
                'priority' => priority::all(),
                'releaseenv' => Releaseenv::all(),
                'status' => 200

            ]
        );
    }

    public function update($id, Request $request)
    {
            $support = Support::find($id);

            $support->notes=$request->notes_edit;
            $support->status_id=$request->status_edit;
            $support->project_id=$request->project_edit;
            $support->releaseenv_id=$request->releaseenv_edit;
            $support->issue_url=$request->issueurl_edit;
            $support->priority_id=$request->priority_edit;
            $support->save();
            if($request->file) {
                foreach ($request->file as $key => $value) {
                 $document = new supportDoucument();
                 $fileName = time() . '-' . $value->getClientOriginalName();
                 $value->move(public_path("support-uploads"), $fileName);
                 $document->Filepath = $fileName;
                 $document->supportrequest_id = $support->id;
                 $document->created_by=Auth::user()->id;
                 $document->save();
                }
             }


            return response()->json([
                'status'=>200 ,
                'support'=> $support,
                'message'=>'successfully'
            ]);


   }
    public function view_support($id){
        $supportrequest=Support::with('Project','Status','Releaseenv','priority')->where('id',$id)->first();
        return view('admin.support.support_view',compact('supportrequest'));
    }
}
