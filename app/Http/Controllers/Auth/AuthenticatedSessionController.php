<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();
        $request->session()->regenerate();
        $user = User::find(auth()->user()->id);
        if ($user->hasPermissionTo('can access dashboard')) {
            return redirect()->intended(RouteServiceProvider::HOME)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access merge requests')) {
            return redirect()->intended(RouteServiceProvider::MERGE)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access support requests')) {
            return redirect()->intended(RouteServiceProvider::SUPPORT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access a project')) {
            return redirect()->intended(RouteServiceProvider::PROJECT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access sprint')) {
            return redirect()->intended(RouteServiceProvider::SPRINT)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can create release env')) {
            return redirect()->intended(RouteServiceProvider::RELEASEENV)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access priority')) {
            return redirect()->intended(RouteServiceProvider::PRIORITY)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access deployment status')) {
            return redirect()->intended(RouteServiceProvider::DEPSTATUS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access quality status')) {
            return redirect()->intended(RouteServiceProvider::QTYSTATUS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access release logs')) {
            return redirect()->intended(RouteServiceProvider::RELEASELOG)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access status')) {
            return redirect()->intended(RouteServiceProvider::STATUS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access users')) {
            return redirect()->intended(RouteServiceProvider::USERS)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access permissions')) {
            return redirect()->intended(RouteServiceProvider::PERMISSION)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else if ($user->hasPermissionTo('can access a roles')) {
            return redirect()->intended(RouteServiceProvider::ROLE)->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.info("Welcome to DevOps Portal");</script>');
        }
        else {
            Auth::logout();
            return redirect("/")->withMessage('<script>toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.error("You Have No Permissions To Access DevOps Portal");</script>');
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->withMessage('<script>toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.success("Logged Out Successfully");</script>');
    }
}
