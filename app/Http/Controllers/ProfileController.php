<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }
        $user = User::find(auth()->user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if($request->hasFile('profile_picture')) {
            if(File::exists($user->profile_picture)){
                File::delete($user->profile_picture);
            }
            $file = $request->profile_picture;
            $fileName = time().'-'.$file->getClientOriginalName();
            $file->move(public_path('profile_picture'),$fileName);
            $user->profile_picture = 'profile_picture/'.$fileName;
        }
        $user->update();
        return Redirect::route('profile.edit')->withMessage('<script>
        toastr.options = {
            "positionClass": "toast-bottom-right",
        }
        toastr.success("Profile is Updated Successfully");
        </script>');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current-password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
