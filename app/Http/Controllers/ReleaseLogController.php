<?php

namespace App\Http\Controllers;

use App\Models\ReleaseLog;
use App\Http\Requests\StoreReleaseLogRequest;
use App\Http\Requests\UpdateReleaseLogRequest;

class ReleaseLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReleaseLogRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReleaseLogRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReleaseLog  $releaseLog
     * @return \Illuminate\Http\Response
     */
    public function show(ReleaseLog $releaseLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReleaseLog  $releaseLog
     * @return \Illuminate\Http\Response
     */
    public function edit(ReleaseLog $releaseLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReleaseLogRequest  $request
     * @param  \App\Models\ReleaseLog  $releaseLog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReleaseLogRequest $request, ReleaseLog $releaseLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReleaseLog  $releaseLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReleaseLog $releaseLog)
    {
        //
    }
}
