<?php

namespace App\Http\Controllers;

use App\Models\QualityStatus;
use App\Http\Requests\StoreQualityStatusRequest;
use App\Http\Requests\UpdateQualityStatusRequest;

class QualityStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreQualityStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQualityStatusRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QualityStatus  $qualityStatus
     * @return \Illuminate\Http\Response
     */
    public function show(QualityStatus $qualityStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QualityStatus  $qualityStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(QualityStatus $qualityStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateQualityStatusRequest  $request
     * @param  \App\Models\QualityStatus  $qualityStatus
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQualityStatusRequest $request, QualityStatus $qualityStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QualityStatus  $qualityStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityStatus $qualityStatus)
    {
        //
    }
}
