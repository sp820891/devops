<?php

namespace App\Http\Controllers;

use App\Models\Depstatus;
use App\Http\Requests\StoreDepstatusRequest;
use App\Http\Requests\UpdateDepstatusRequest;

class DepstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDepstatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepstatusRequest $request)
    {
        $request->validated();
        $dep_status = new Depstatus;
        $dep_status->name = $request->name;
        $dep_status->description = $request->description;
        $dep_status->created_by = auth()->user()->id;
        $dep_status->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Depstatus  $depstatus
     * @return \Illuminate\Http\Response
     */
    public function show(Depstatus $depstatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Depstatus  $depstatus
     * @return \Illuminate\Http\Response
     */
    public function edit(Depstatus $depstatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDepstatusRequest  $request
     * @param  \App\Models\Depstatus  $depstatus
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepstatusRequest $request, Depstatus $depstatus)
    {
        $request->validated();
        $depstatus->name = $request->name;
        $depstatus->description = $request->description;
        $depstatus->update();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Depstatus  $depstatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Depstatus $depstatus)
    {
        $depstatus->delete();
        return redirect()->back();
    }
}
