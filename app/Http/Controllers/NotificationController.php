<?php

namespace App\Http\Controllers;


use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function merg()
    {
        $merg_details = Notification::orderBy('created_at','desc')->get();
        $count = Notification::count();
        return response()->json(['status'=>200,'merg'=>$merg_details,'count'=>$count]);
    }
}
