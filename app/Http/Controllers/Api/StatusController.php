<?php

namespace App\Http\Controllers\Api;
use App\Models\Status;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
class StatusController extends Controller
{
    public function status(){
        $status=Status::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete status')) {
            return response()->json([
                'status'=>200,
                'statuses'=>$status,
                'permission'=> 'can delete status'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'statuses'=>$status,
            ]);
        }
       }
    public function fetchStatusId($id)
    {
        $status = Status::find($id);
        return response()->json(
            [
                'statuses' => $status,
                'status' => 200
            ]
        );
    }
       public function storeStatus(Request $request){
       $request->validate([
            'status_name'=>'required|string|max:191',
            'status_description'=>'required|string|max:191',
        ]);
        $status=Status::create([
            'name'=>$request->status_name,
            'description'=>$request->status_description,
            'created_by'=> auth()->user()->id,
        ]);
        if($status){
            return response()->json([
                'status'=>200,
            ]);
        }else{
            return response()->json([
                'status'=>404,
            ]);
        }
     }
    public function deleteStatus($id){
         $status=Status::find($id);
         if($status){
             $status->delete();
             return response()->json([
                 'status'=>200,
             ]);
         }else{
             return response()->json([
                 'status'=>404,
             ]);
         }
     }

}
