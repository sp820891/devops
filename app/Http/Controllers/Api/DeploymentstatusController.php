<?php

namespace App\Http\Controllers\Api;
use App\Models\Depstatus;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeploymentstatusController extends Controller
{
    public function deploymentstatus(){
        $dep_status=Depstatus::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete deployment status')) {
            return response()->json([
                'status'=>200,
                'dep_status'=>$dep_status,
                'permission'=> 'can delete deployment status'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'dep_status'=>$dep_status,
            ]);
        }
    }
    public function fetchDepStatusId($id)
    {
        $dep_status = Depstatus::find($id);
        return response()->json(
            [
                'dep_status' => $dep_status,
                'status' => 200

            ]
        );
    }
    public function storeDeploymentstatus(Request $request){
       $request->validate([
            'deployment_status_name'=>'required|string|max:191',
            'deployment_status_description'=>'required|string|max:191',
        ]);
        $dep_status=Depstatus::create([
            'name'=>$request->deployment_status_name,
            'description'=>$request->deployment_status_description,
            'created_by'=> auth()->user()->id,
        ]);
        if($dep_status){
            return response()->json([
                'status'=>200,
            ]);
        }else{
            return response()->json([
                'status'=>404,
            ]);
        }
    }
     public function deleteDeploymentstatus($id){
         $users=Depstatus::find($id);
         if($users){
             $users->delete();
             return response()->json([
                 'status'=>200,
                 'message'=>"student record delete successfully"
             ],200);
         }else{
             return response()->json([
                 'status'=>404,
                 'message'=>"No such student Found"
             ],404);
         }
     }
}
