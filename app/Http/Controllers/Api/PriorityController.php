<?php

namespace App\Http\Controllers\Api;
use App\Models\Priority;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PriorityController extends Controller
{
    public function priority(){
        $priority=Priority::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete priority')) {
            return response()->json([
                'status'=>200,
                'priority'=>$priority,
                'permission'=> 'can delete priority'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'priority'=>$priority,
            ]);
        }
    }
    public function fetchPriorityId($id)
    {
        $priority = Priority::find($id);
        return response()->json(
            [
                'priority' => $priority,
                'status' => 200

            ]
        );
    }
    public function storePriority(Request $request){
        $request->validate([
            'priority_name'=>'required|string|max:191',
            'priority_description'=>'required|string|max:191',
        ]);
        $priority=Priority::create([
            'name'=>$request->priority_name,
            'description'=>$request->priority_description,
            'created_by'=> auth()->user()->id,
        ]);
        if($priority){
        return response()->json([
            'status'=>200,
        ]);
        }else{
        return response()->json([
            'status'=>404,
        ]);
        }
     }
    public function deletePriority($id){
         $priority=Priority::find($id);
         if($priority){
             $priority->delete();
             return response()->json([
                 'status'=>200,
             ]);
         }else{
        return response()->json([
            'status'=>404,
        ]);
         }
     }
}
