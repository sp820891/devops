<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
class apiController extends Controller
{
    /*------------------------------------------------ USER-API-------------------------------*/
    public function user(){
        $users=User::all();
        if($users->count()>0){
            return response()->json([
                'status'=>200,
                'users'=>$users
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'users'=>"No record found"
            ],404);
           }
       }
       public function storeUser(Request $request){
        $validator= validator::make($request->all(),[
            'first_name'=>'required|string|max:191',
            'last_name'=>'required|string|max:191',
            'gender'=>'required|string|max:191',
            'email'=>'required|string|max:191',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);
        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ],422);
        }else{
            $user=User::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'gender'=>$request->gender,
                'email'=>$request->email,
                'password'=>bycrypt($request->password),
                'password_confirmation'=>bycrypt($request->password_confirmation),

            ]);
            if($user){
                return response()->json([
                    'status'=>200,
                    'message'=>"student Created successfully"
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"someting went wrong"
                ],200);
            }
        }
     }
     public function showUser($id){
        $users=User::find($id);
        if($users){
            return response()->json([
                'status'=>200,
                'users'=>$users
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such student Found"
            ],404);
        }
     }
     public function editUser($id){
        $users=User::find($id);
        if($users){
            return response()->json([
                'status'=>200,
                'users'=>$users
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such student Found"
            ],404);
        }
     }
     public function updateUser(Request $request, $id)
    {
        $validator= validator::make($request->all(),[
            'first_name'=>'required|string|max:191',
            'last_name'=>'required|string|max:191',
            'gender'=>'required|string|max:191',
            'email'=>'required|string|max:191',
            'password' =>  'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6',
        ]);
        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ],422);
        }else{
            $user=User::find($id);
            if($user){
                $user->update([
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'gender'=>$request->gender,
                    'email'=>$request->email,
                    'password'=>$request->password,
                    'password_confirmation'=>$request->password_confirmation,
                ]);
                return response()->json([
                    'status'=>200,
                    'message'=>"student Updated successfully"
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=>"No such student Found"
                ],404);
            }
        }
    }
    public function deleteUser($id){
        $users=User::find($id);
        if($users){
            $users->delete();
            return response()->json([
                'status'=>200,
                'message'=>"student record delete successfully"
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such student Found"
            ],404);
        }
    }

}
