<?php

namespace App\Http\Controllers\Api;
use App\Models\ReleaseEnv;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ReleaseenvController extends Controller
{
    public function Releaseenv(){
        $envs=ReleaseEnv::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete release env')) {
            return response()->json([
                'status'=>200,
                 'envs'=>$envs,
                'permission'=> 'can delete release env'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                 'envs'=>$envs,
            ]);
        }
    }
    public function fetchEnvId($id)
    {
        $env = ReleaseEnv::find($id);
        return response()->json(
            [
                'env' => $env,
                'status' => 200

            ]
        );
    }
    public function storeReleaseenv(Request $request){
    $request->validate([
        'release_env_name'=>'required|string|max:191',
        'release_env_description'=>'required|string|max:191',
    ]);
        $env=ReleaseEnv::create([
            'name'=>$request->release_env_name,
            'description'=>$request->release_env_description,
            'created_by'=> auth()->user()->id,
        ]);
        if($env){
            return response()->json([
                'status'=>200,
            ],);
        }else{
            return response()->json([
                'status'=>404,
            ]);
        }
     }
     public function deleteReleaseenv($id){
         $env=ReleaseEnv::find($id);
         if($env){
             $env->delete();
             return response()->json([
                 'status'=>200,
             ]);
         }else{
             return response()->json([
                 'status'=>404,
             ]);
         }
     }
}
