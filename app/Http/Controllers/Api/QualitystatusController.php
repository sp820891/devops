<?php
namespace App\Http\Controllers\Api;
use App\Models\QualityStatus;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class QualitystatusController extends Controller
{
    public function qualitystatus(){
        $qty_status=QualityStatus::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete quality status')) {
            return response()->json([
                'status'=>200,
                'qty_status'=>$qty_status,
                'permission'=> 'can delete quality status'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'qty_status'=>$qty_status,
            ]);
        }
       }
    public function fetchQtyStatusId($id)
    {
        $qty_status = QualityStatus::find($id);
        return response()->json(
            [
                'qty_status' => $qty_status,
                'status' => 200
            ]
        );
    }
    public function storeQualitystatus(Request $request){
    $request->validate([
        'quality_status_name'=>'required|string|max:191',
        'quality_status_description'=>'required|string|max:191',
    ]);
    $qty_status=QualityStatus::create([
        'name'=>$request->quality_status_name,
        'description'=>$request->quality_status_description,
        'created_by'=> auth()->user()->id,
    ]);
    if($qty_status){
        return response()->json([
            'status'=>200,
        ]);
    }else{
        return response()->json([
            'status'=>404,
        ]);
    }
    }
    public function deleteQualitystatus($id){
        $qty_status=QualityStatus::find($id);
        if($qty_status){
            $qty_status->delete();
            return response()->json([
                'status'=>200,
            ]);
        }else{
            return response()->json([
                'status'=>404,
            ]);
        }
    }
}
