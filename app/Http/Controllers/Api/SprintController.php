<?php

namespace App\Http\Controllers\Api;
use App\Models\Sprint;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class SprintController extends Controller
{
    public function sprint(){
        $sprints=Sprint::with("users")->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete sprint')) {
            return response()->json([
                'status'=>200,
                'sprints'=>$sprints,
                'permission'=> 'can delete sprint'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'sprints'=>$sprints,
            ]);
        }
        }
    public function fetchSprintId($id)
    {
        $sprint = Sprint::find($id);
        return response()->json(
            [
                'sprint' => $sprint,
                'status' => 200

            ]
        );
    }
    public function storeSprint(Request $request){
    $request->validate([
        'sprint'=>'required|int|max:191|unique:sprints,name',
        'start_date'=>'required|date|max:191',
        'end_date'=>'required|date|max:191',
        'goal'=>'required|string|max:191',
    ]);
    $sprints=Sprint::create([
        'name'=>$request->sprint,
        'start_date'=>$request->start_date,
        'end_date'=>$request->end_date,
        'goal'=>$request->goal,
        'owner'=> auth()->user()->id,
    ]);
    if($sprints){
        return response()->json([
            'status'=>200,
        ]);
    }else{
        return response()->json([
            'status'=>404,
        ]);
    }
    }
    public function deleteSprint($id){
    $sprint=Sprint::find($id);
    if($sprint){
        $sprint->delete();
        return response()->json([
            'status'=>200,
        ]);
    }else{
        return response()->json([
            'status'=>404,
        ]);
    }
    }
}
