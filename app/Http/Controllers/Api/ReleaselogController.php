<?php

namespace App\Http\Controllers\Api;
use App\Models\ReleaseLog;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class ReleaselogController extends Controller
{
    public function releaselog(){
        $logs=ReleaseLog::with('mergerequest','users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete release logs')) {
            return response()->json([
                'status'=>200,
                'logs'=>$logs,
                'permission'=> 'can delete release logs'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'logs'=>$logs,
            ]);
        }
       }
       public function fetchLogsId($id)
       {
        $logs = ReleaseLog::find($id);
        return response()->json(
            [
                'logs' => $logs,
                'status' => 200
            ]
        );
       }
       public function deleteReleaseLog($id){
        $logs=ReleaseLog::find($id);
        if($logs){
            $logs->delete();
            return response()->json([
                'status'=>200,
            ]);
        }else{
            return response()->json([
                'status'=>404,
            ]);
        }
    }
}

