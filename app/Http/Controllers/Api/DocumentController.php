<?php

namespace App\Http\Controllers\Api;
use App\Models\Document;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class DocumentController extends Controller
{
    public function document(){
        $users=Document::all();
        if($users->count()>0){
            return response()->json([
                'status'=>200,
                'users'=>$users
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'users'=>"No record found"
            ],404);
           }
       }
       public function storeDocument(Request $request){
        $validator= validator::make($request->all(),[
            'mergerequest_id '=>'required|int|max:191',
            'name'=>'required|string|max:191',
            'description'=>'required|string|max:191',
            'path'=>'required|string|max:2048',
            'status'=>'required|int|max:191',
            'created_by'=>'required|int|max:191',
        ]);
        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors'=>$validator->messages()
            ],422);
        }else{
            $user=Document::create([
                'mergerequest_id'=>$request->mergerequest_id,
                'name'=>$request->name,
                'description'=>$request->description,
                'path'=>$request->path,
                'status'=>$request->status,
                'created_by'=>$request->created_by,
            ]);
            if($user){
                return response()->json([
                    'status'=>200,
                    'message'=>"student Created successfully"
                ],200);
            }else{
                return response()->json([
                    'status'=>500,
                    'message'=>"someting went wrong"
                ],200);
            }
        }
     }
     public function updateDocument(Request $request, $id)
     {
         $validator= validator::make($request->all(),[
            'mergerequest_id '=>'required|int|max:191',
            'name'=>'required|string|max:191',
            'description'=>'required|string|max:191',
            'path'=>'required|string|max:2048',
            'status'=>'required|int|max:191',
            'created_by'=>'required|int|max:191',
         ]);
         if($validator->fails()){
             return response()->json([
                 'status'=>422,
                 'errors'=>$validator->messages()
             ],422);
         }else{
             $user=Document::find($id);
             if($user){
                 $user->update([
                    'mergerequest_id'=>$request->mergerequest_id,
                    'name'=>$request->name,
                    'description'=>$request->description,
                    'path'=>$request->path,
                    'status'=>$request->status,
                    'created_by'=>$request->created_by,
                 ]);
                 return response()->json([
                     'status'=>200,
                     'message'=>"student Updated successfully"
                 ],200);
             }else{
                 return response()->json([
                     'status'=>404,
                     'message'=>"No such student Found"
                 ],404);
             }
         }
     }
     public function deleteDocument($id){
        $users=Document::find($id);
        if($users){
            $users->delete();
            return response()->json([
                'status'=>200,
                'message'=>"student record delete successfully"
            ],200);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>"No such student Found"
            ],404);
        }
    }
}
