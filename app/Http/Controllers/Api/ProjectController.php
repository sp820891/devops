<?php

namespace App\Http\Controllers\Api;
use App\Models\Project;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function Project(){
        $projects=Project::with('users')->get();
        $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete a project')) {
            return response()->json([
                'status'=>200,
                'projects'=>$projects,
                'permission'=> 'can delete a project'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'projects'=>$projects,
            ]);
        }
    }
    public function fetchProjectId($id)
    {
        $project = Project::find($id);
        return response()->json(
            [
                'project' => $project,
                'status' => 200
            ]
        );
    }
    public function storeProject(Request $request){
    $request->validate([
        'project_name'=>'required|string|max:191',
        'project_description'=>'required|string|max:191',
    ]);
    $project=Project::create([
        'name'=>$request->project_name,
        'description'=>$request->project_description,
        'created_by'=> auth()->user()->id
    ]);
    if($project){
        return response()->json([
            'status'=>200,
        ]);
    }else{
    return response()->json([
        'status'=>404,
    ]);
    }
    }
    public function deleteProject($id){
        $project=Project::find($id);
        if($project){
            $project->delete();
            return response()->json([
                'status'=>200,
            ]);
        }else{
        return response()->json([
            'status'=> 404,
        ]);
        }
    }
}
