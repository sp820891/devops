<?php

namespace App\Http\Controllers;

use App\Models\Mergerequest;
use Illuminate\Support\Facades\File;
use Spatie\FlareClient\View;
use Illuminate\Support\Facades\Auth;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Depstatus;
use App\Models\Status;
use App\Models\priority;
use App\Models\Project;
use App\Models\Qualitystatus;
use App\Models\Releaseenv;
use App\Models\Sprint;
use App\Models\MergNotification;
use App\Models\Notification;
use App\Models\Releaselog;
use App\Models\User;
use App\Providers\MergCreated;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Log;

class MergerequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMergerequestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'git_test_link' => 'required',
            'issue_url' => 'required',
        ]);
            $merge = new Mergerequest();
            $user = Auth::user()->id;
            $merge->user_id = $user;
            $merge->project_id = 1;
            $merge->git_test_link = $request->input('git_test_link');
            $merge->sql_update_link = $request->input('sqllink');
            $merge->env_update_link = $request->input('envlink');
            $merge->status_id = 4;
            $merge->priority_id = $request->input('priority');
            $merge->releaseenv_id = 1;
            $today_date = Carbon::now();
            $sprint = Sprint::whereDate('start_date','<=',$today_date)->whereDate('end_date','>=',$today_date)->first();
            $merge->sprint_id = $sprint->id;
            $merge->artisan_command = $request->input('artisan');
            $merge->issue_url = $request->input('issue_url');
            $merge->save();
            if($request->file) {
               foreach ($request->file as $key => $value) {
                $document = new Document();
                $fileName = time() . '-' . $value->getClientOriginalName();
                $value->move(public_path("uploads"), $fileName);
                $document->path = $fileName;
                $document->mergerequest_id = $merge->id;
                $document->created_by=$user;
                $document->save();
               }
            }
            $notification = Mergerequest::all()->last();
            $username = User::find($notification->user_id);
            $merg = new Notification();
            $merg->username = $username->first_name;
            $merg->merg_id = $notification->id;
            $merg->save();
            event(new MergCreated($merg));
            return response()->json(['status'=>200 ,'date'=>$today_date,'message'=>'request','url' => url("view_mergerequests/".$merge->id)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mergerequest  $mergerequest
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $mergerequest = Mergerequest::with('projects','user')->orderBy('created_at' ,'desc')->get();
        return response()->json([
            'status' => 200,
            'merge' => $mergerequest
        ]);
    }

    public function view_merge($id){
        $merge = Mergerequest::find($id);
        $sprint = Sprint::join('mergerequests','mergerequests.sprint_id','=','sprints.id')->where('mergerequests.id',$id)->first();
        $project = Project::join('mergerequests','mergerequests.project_id','=','projects.id')->where('mergerequests.id',$id)->first();
        $priority = Priority::join('mergerequests','mergerequests.priority_id','=','priorities.id')->where('mergerequests.id',$id)->first();
        $env = Releaseenv::join('mergerequests','mergerequests.releaseenv_id','=','releaseenvs.id')->where('mergerequests.id',$id)->first();
        $status = Status::join('mergerequests','mergerequests.status_id','=','statuses.id')->where('mergerequests.id',$id)->first();
        return view('admin.merge.view_merge',compact('merge','sprint','project','priority','env','status'));
       }

       public function updateComment($id,Request $request){
        $name = auth()->user()->id;
        $comment= new Comment();
        $comment->mergerequest_id =$id;
        $comment->comment=$request->comment;
        $comment->created_by=$name;
        $comment->save();
        return response()->json(
            [
                'status'=>200,
                //'comments' => Comment::all(),
            ]
            );
    }
    public function showComment($id){
        //Log::info("Comment details: ".$id);
        $comment=Comment::with('users')->where('mergerequest_id',$id)->orderBy('id','desc')->get();
            if($comment->count()>0){
                return response()->json([
                    'status'=>200,
                    'comment'=>$comment,
                    'auth' => auth()->user()->first_name,
                ]);
            }else{
                return response()->json([
                    'status'=>404,
                    'comment'=>"Start Coversation"
                ]);
               }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mergerequest  $mergerequest
     * @return \Illuminate\Http\Response
     */
      public function edit(Request $request)
    {
        $merge = Mergerequest::find($request->merge);
        return response()->json(
            [
                'merge' => $merge,
                'statuses' => Status::all(),
                'priority' => priority::all(),
                'releaseenv' => Releaseenv::all(),
                'status' => 200
            ]
        );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMergerequestRequest  $request
     * @param  \App\Models\Mergerequest  $mergerequest
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
            $merge = Mergerequest::with("projects")->find($id);

            if ($request->status == 1) {
                $testcount = $merge->test_release_count + 1;
                $testdone = $merge->test_release_date = Carbon::today();
            } else {
                $testcount = $merge->test_release_count;
                $testdone = $merge->test_release_date;
            }
            if ($request->status == 2) {

                $prodcount = $merge->prod_release_count + 1;
                $proddone = $merge->prod_release_date = Carbon::today();
            } else {
                $prodcount = $merge->prod_release_count;
                $proddone = $merge->prod_release_date;
            }

            $merge->project_id = $request->project;
            $merge->git_test_link = $request->testlink;
            $merge->git_prod_link = $request->prodlink;
            $merge->sql_update_link = $request->sqllink;
            $merge->env_update_link = $request->envlink;
            $merge->status_id = $request->status;
            $merge->priority_id = $request->priority;
            $merge->releaseenv_id = $request->releaseenv;
            $merge->quality_score = $request->qscore;
            $merge->sprint_id = $request->sprint;
            $merge->artisan_command = $request->artisan;
            $merge->issue_url = $request->issueurl;
            $merge->depstatus_id = $request->depstatus;
            $merge->qualitystatus_id = $request->quality;
            $merge->prod_release_count = $prodcount;
            $merge->test_release_count =  $testcount;
            $merge->test_release_date =  $testdone;
            $merge->prod_release_date =  $proddone;

            if($request->file) {
                foreach ($request->file as $key => $value) {
                 $document = new Document();
                 $fileName = time() . '-' . $value->getClientOriginalName();
                 $value->move(public_path("uploads"), $fileName);
                 $document->path = $fileName;
                 $document->mergerequest_id = $merge->id;
                 $document->created_by=Auth::user()->id;
                 $document->save();
                }
            }
            if ($request->status == "3") {
                $rejectcount = $merge->reject_count + 1;
                $reject = new Releaselog();
                $reject->mergerequest_id = $merge->id;
                $reject->created_by = Auth::user()->id;
                $reject->save();
                $merge->reject_count = $rejectcount;
                $merge->save();
                $comment = new Comment();
                $comment->mergerequest_id = $id;
                $comment->comment = $request->comment;
                $comment->created_by = auth()->user()->id;
                $comment->save();
                $notification = Mergerequest::find($id);
                $merg = new Notification();
                $merg->merg_id = $notification->id;
                $merg->user_id = $notification->user_id;
                $merg->rejected_by = auth()->user()->first_name;
                $merg->save();

                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "3",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            } else {
                $rejectcount = $merge->reject_count;
                $merge->reject_count = $rejectcount;
                $merge->save();
            }


            if($request->status == "1") {
                $merge->release_by = auth()->user()->id;
                $merge->save();
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "1",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else if($request->status == "2") {
                $merge->release_by = auth()->user()->id;
                $merge->save();
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "2",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else if($request->status == "4") {
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "4",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else if($request->status == "5") {
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "5",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else if($request->status == "6") {
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "6",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else if($request->status == "7") {
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'status' => "7",
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
            else {
                return response()->json(
                    [
                        'status' => 200,
                        'merge' => $merge,
                        'url' => url("view_mergerequests/".$merge->id),
                    ]
                    );
            }
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mergerequest  $mergerequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        foreach ($request->merge as $key => $value) {
            $merge = Mergerequest::find($value);
            ReleaseLog::where('mergerequest_id',$value)->delete();
            Document::where('mergerequest_id',$value)->delete();
            $merge->delete();
        }

        return response()->json([

        'status' => 200,

        ]);
    }
}
