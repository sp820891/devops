<?php

namespace App\Http\Controllers;

use App\Models\Mergerequest;
use App\Models\Releaselog;
use App\Reports\MyReport;
use App\Reports\RejectReport;
use App\Reports\SprintReport;
use App\Reports\EnvReport;

use Illuminate\Http\Request;
use Psy\Readline\Hoa\Console;
use DB;
use Illuminate\Console\View\Components\Alert;
use Illuminate\Support\Facades\DB as FacadesDB;

class MergReportController extends Controller
{
    // graph using the koolreport

    // public function __contruct () {
    //     $this->middleware("auth");
    // }
    // public function merg ($id) {
    //     // $switch =  $id;
    //     // dd($switch);
    //     if ($id = 1) {
    //         dd('hellow');
    //         $report = new MyReport();
    //         $report->run();
    //         return view("admin.dashboard",compact(['report','switch']));
    //     }
    //     elseif($switch = '2')
    //     {
    //         dd('hi');
    //         $reject = new RejectReport();
    //         $reject->run();
    //         return view("admin.dashboard",compact(['reject','switch']));
    //     }
    //     elseif($switch = '3')
    //     {
    //         dd('how r u');
    //         $sprint = new SprintReport();
    //         $sprint->run();
    //         return view("admin.dashboard",compact(['sprint','switch']));
    //     }
    //      else
    //     {
    //         $env = new EnvReport();
    //         $env->run();
    //         return view("admin.dashboard",compact(['env','switch']));
    //     }

    // }


    public function merg($id)
    {
            if($id == 1)
            {
                $data = FacadesDB::SELECT("SELECT COUNT(*) Count,u.first_name FROM mergerequests mr INNER JOIN users u ON mr.user_id = u.id GROUP BY mr.user_id, u.first_name");
                return response()->json(['dataPoints' => $data]);
            }
            elseif($id == 2)
            {
                $reject = FacadesDB::select("SELECT reject_count,u.first_name FROM mergerequests mr INNER JOIN users u ON mr.release_by = u.id  GROUP BY mr.user_id,mr.reject_count,u.first_name");
                return response()->json(['dataPoints' => $reject ]);
            }
            elseif($id == 3)
            {
                $test_release_count = FacadesDB::select("SELECT SUM(`test_release_count`) Count ,p.name FROM mergerequests mr  JOIN projects p ON mr.project_id = p.id GROUP BY project_id,p.name ;");
                return response()->json(['dataPoints' => $test_release_count]);
            }
            else
            {
                $prod_release_count = FacadesDB::select("SELECT SUM(`prod_release_count`) Count ,p.name FROM mergerequests mr JOIN projects p ON mr.project_id = p.id GROUP BY project_id,p.name;");
                return response()->json(['dataPoints' => $prod_release_count ]);
            }
    }

}
