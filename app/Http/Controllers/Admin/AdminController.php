<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserCreated;
use App\Http\Controllers\Controller;
use App\Mail\NotificationMailler;
use App\Mail\UserMailler;
use App\Models\User;
use App\Reports\MyReport;
use App\Models\Audit;
use App\Models\Depstatus;
use App\Models\Mergerequest;
use App\Models\priority;
use App\Models\Project;
use App\Models\Qualitystatus;
use App\Models\Sprint;
use App\Models\Status;
use App\Models\Releaseenv;
use App\Models\supportDoucument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\Permission;
use App\Models\Releaselog;
use App\Models\Role;

class AdminController extends Controller
{
    public function admin()
    {
        return view('admin.layouts.admin');
   }

   public function user(){
      $roles = Role::all();
      $log = Audit::with('user')->where('auditable_type','App\Models\User')->orderBy('created_at', 'desc')->get();
      return view('admin.security.users',compact('log','roles'));
   }

   public function dashboard()
   {
     $merge = Mergerequest::all();
     $reject_count = Releaselog::count();
     $test_count = Mergerequest::sum('test_release_count');
     $prod_count = Mergerequest::sum('prod_release_count');
     return view('admin.dashboard.dashboard',compact(['merge','reject_count','test_count','prod_count']));
   }
   public function merge(){
    $project=Project::all();
    $sprint=Sprint::all();
    $status=Status::all();
    $priority=priority::all();
    $env=Releaseenv::all();
   $log = Audit::with('user')->where('auditable_type','App\Models\Mergerequest')->orderBy('created_at', 'desc')->get();
    return view('admin.merge.mergerequest',compact('project','sprint','status','priority','env','log'));
   }
   public function support()
    {
        $project=Project::all();
        $status=Status::all();
        $priority=priority::all();
        $env=Releaseenv::all();
        $supportdocument=supportDoucument::all();
        $log = Audit::with('user')->where('auditable_type','App\Models\support')->orderBy('created_at', 'desc')->get();
        return view('admin.support.support_request',compact('project','status','priority','env','supportdocument','log'));
    }
   //Users CRUD
   public function fetchUser()
   {
    $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can access user based permissions') && $user->hasPermissionTo('can edit users') && $user->hasPermissionTo('can delete users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission1' => 'can access user based permissions',
                'permission2' => 'can edit users',
                'permission3' => 'can delete users',
            ]);
        }
        else if($user->hasPermissionTo('can access user based permissions') && $user->hasPermissionTo('can edit users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission1' => 'can access user based permissions',
                'permission2' => 'can edit users',
            ]);
        }
        else if($user->hasPermissionTo('can edit users') && $user->hasPermissionTo('can delete users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission2' => 'can edit users',
                'permission3' => 'can delete users',
            ]);
        }
        else if($user->hasPermissionTo('can access user based permissions') && $user->hasPermissionTo('can delete users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission1' => 'can access user based permissions',
                'permission3' => 'can delete users',
            ]);
        }
        else if($user->hasPermissionTo('can access user based permissions')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission1' => 'can access user based permissions',
            ]);
        }
        else if($user->hasPermissionTo('can edit users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission2' => 'can edit users',
            ]);
        }
        else if($user->hasPermissionTo('can delete users')) {
            return response()->json([
                'status'=>200,
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'permission3' => 'can delete users',
            ]);
        }
        else {
            return response()->json([
                'users' => User::where('first_name' , '!=' , auth()->user()->first_name)->where('id' , '!=' , "1")->with('roles')->get(),
                'status' => 200
            ]);
        }
   }
   public function addUser(Request $request)
   {
       $request->validate([
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'gender' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed'],
            'role_as' => 'required',
       ]);
       $user = User::create([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'gender' => $request->gender,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        ]);
        $user->assignRole($request->role_as);
        // event
        // $name = $request->all();
        // event(new UserCreated($name));
        if($user){
            return response()->json(
                [
                    'user' => $user,
                    'role' => $user->roles->pluck('name')->first(),
                    'name' => $request->first_name,
                    'email' => $request->email,
                    'status' => 200
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }

   }
   public function editUser($id)
   {
    $user = User::find($id);
    return response()->json(
        [
            'user' => $user,
            'role' => $user->roles->pluck('name')->first(),
            'status' => 200,
            'roles' => Role::all(),
        ]
    );
   }
   public function updateUser($id, Request $request)
   {
    $request->validate([
        'first_name' => ['required', 'string'],
        'last_name' => ['required', 'string'],
        'gender' => ['required', 'string'],
        'email' => ['required', 'string', 'email', 'max:255'],
        'role_as' => 'required',
   ]);
   $user = User::find($id);
   $user->update([
    'first_name' => $request->first_name,
    'last_name' => $request->last_name,
    'gender' => $request->gender,
    'email' => $request->email,
    ]);
    $user->removeRole($user->roles->pluck('name')->first());
    $user->assignRole($request->role_as);
    if($user){
        return response()->json(
            [
                'status' => 200,
                'user' => $user,
                'email' => $request->email,
            ]
        );
    }
    else{
        return response()->json([
            'status'=>404,
        ]);
    }
   }
   public function deleteUser($id)
   {
        $user = User::find($id)->delete();
        if($user){
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
   public function setPassword($token , $email)
   {
     return view('auth.reset-password',compact('token','email'));
   }
   //Roles CRUD
   public function fetchRole()
   {
    $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can access role based permissions') && $user->hasPermissionTo('can delete roles')) {
            return response()->json([
                'status'=>200,
                'roles' => Role::all(),
                'permission1'=> 'can access role based permissions',
                'permission2'=> 'can delete roles'
            ]);
        }
        else if($user->hasPermissionTo('can access role based permissions')) {
            return response()->json([
                'status'=>200,
                'roles' => Role::all(),
                'permission1'=> 'can access role based permissions',
            ]);
        }
        else if($user->hasPermissionTo('can delete roles')) {
            return response()->json([
                'status'=>200,
                'roles' => Role::all(),
                'permission2'=> 'can delete roles'
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'roles' => Role::all(),
            ]);
        }
    }
   public function addRole(Request $request)
   {
       $request->validate([
            'name' => ['required', 'string'],
            'guard_name' => ['required', 'string'],
       ]);
         $role = Role::create([
        'name' => $request->name,
        'guard_name' => $request->guard_name,
        ]);
        if($role){
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
   public function viewRole($id)
   {
    $role = Role::find($id);
    return response()->json(
        [
            'role' => $role,
            'status' => 200,
        ]
    );
   }
   public function deleteRole($id)
   {
        $role = Role::find($id)->delete();
        if($role){
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
   //Permissions CRUD
   public function fetchPermission()
   {
    $user = User::find(auth()->user()->id);
        if($user->hasPermissionTo('can delete permissions')) {
            return response()->json(
                [
                    'permissions' => Permission::all(),
                    'status' => 200,
                    'permission'=> 'can delete permissions'
                ]
            );
        }
        else {
            return response()->json(
                [
                    'permissions' => Permission::all(),
                    'status' => 200,
                ]
            );
        }
   }
   public function addPermission(Request $request)
   {
       $request->validate([
            'name' => ['required', 'string'],
            'guard_name' => ['required', 'string'],
       ]);
        $permission = Permission::create([
        'name' => $request->name,
        'guard_name' => $request->guard_name,
        ]);
        if($permission){
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
   public function viewPermission($id)
   {
    $permission = Permission::find($id);
    return response()->json(
        [
            'permission' => $permission,
            'status' => 200,
        ]
    );
   }
   public function deletePermission($id)
   {
        $permission = Permission::find($id)->delete();
        if($permission){
            return response()->json(
                [
                    'status' => 200,
                ]
            );
        }
        else{
            return response()->json([
                'status'=>404,
            ]);
        }
   }
   public function project()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\Project')->orderBy('created_at', 'desc')->get();
    return view('admin.project.project',compact('log'));
   }
   public function permissions()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\Permission')->orderBy('created_at', 'desc')->get();
    return view('admin.security.permissions',compact('log'));
   }

   public function sprint()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\Sprint')->orderBy('created_at', 'desc')->get();
    return view('admin.sprint.sprint',compact('log'));
   }

   public function releaseenv()
   {
   $log = Audit::with('user')->where('auditable_type','App\Models\ReleaseEnv')->orderBy('created_at', 'desc')->get();
    return view('admin.release.releaseenv',compact('log'));
   }

   public function priority()
   {
   $log = Audit::with('user')->where('auditable_type','App\Models\Priority')->orderBy('created_at', 'desc')->get();
    return view('admin.priority.priority',compact('log'));
   }

   public function deployment()
   {
  $log = Audit::with('user')->where('auditable_type','App\Models\Depstatus')->orderBy('created_at', 'desc')->get();
    return view('admin.deployment.deployment',compact('log'));
   }

   public function role()
   {
     $log = Audit::with('user')->where('auditable_type','App\Models\Role')->orderBy('created_at', 'desc')->get();
       return view('admin.security.role',compact('log'));
   }

   public function releaselog()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\ReleaseLog')->orderBy('created_at', 'desc')->get();
    return view('admin.release.releaselog',compact('log'));
   }

   public function qualitystatus()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\QualityStatus')->orderBy('created_at', 'desc')->get();
    return view('admin.qualitystatus.qualitystatus',compact('log'));
   }
   public function comment()
   {
       return view('admin.merge.comment');
   }
   public function commentType()
   {
       return view('admin.merge.comment_type');
   }
   public function status()
   {
    $log = Audit::with('user')->where('auditable_type','App\Models\Status')->orderBy('created_at', 'desc')->get();
    return view('admin.status.status',compact('log'));
   }
   public function rbp($id)
   {
    $role = Role::findById($id);
    $Permissions = Permission::all();
    return view('admin.security.permissionandroles',compact('role','Permissions'));
   }
   public function ubp($id)
   {
    $user = User::find($id);
    $Permissions = $user->getPermissionsViaRoles();
    $allPermissions = Permission::all();
    return view('admin.security.userandpermission',compact('user','allPermissions','Permissions'));
   }
   public function fetchRoleWithPermission($role_name)
   {
      $role = Role::findByName($role_name);
      $permissions = $role->permissions->pluck('id');
      return response()->json([
          'permissions' => $permissions,
          'status' => 200
      ]);
   }
   public function assignRoleWithPermission(Request $request,$role_name)
   {
        $role = Role::findByName($role_name);
        foreach ($request->checked as $key => $value) {
            $permission = Permission::findByName($value);
            $role->givePermissionTo($permission);
        }
        if($request->unchecked != "") {
            foreach ($request->unchecked as $key => $value) {
                $permission = Permission::findByName($value);
                $role->revokePermissionTo($permission);
            }
       }
        return response()->json([
            'status' => 200,
        ]);
   }
   public function fetchUserWithPermission($user_id)
   {
      $user = User::find($user_id);
      $permissions = $user->permissions->pluck('id');
      $rolePermissions = $user->getPermissionsViaRoles();
      $userPermissions = $user->getAllPermissions();
      $diffPermissions = $userPermissions->diff($rolePermissions);
      return response()->json([
          'permissions' => $permissions,
          'userPermissions' => $diffPermissions,
          'rolePermissions' => $rolePermissions,
          'status' => 200
      ]);
   }
   public function assignUserWithPermission(Request $request,$user_id)
   {
        $user = User::find($user_id);
        foreach ($request->checked as $key => $value) {
            $permission = Permission::findByName($value);
            $user->givePermissionTo($permission);
        }
        if($request->unchecked != "") {
            foreach ($request->unchecked as $key => $value) {
                $permission = Permission::findByName($value);
                $user->revokePermissionTo($permission);
            }
        }
        return response()->json([
            'status' => 200,
        ]);
   }
   public function userCredentialMailler(Request $request)
   {
      //Mail For User Credentials
      $name = $request->name;
      $email = $request->email;
      Mail::to("$request->email")->send(new UserMailler($name,$email));
   }
   public function mailAndNotification(Request $request)
   {
        // Mail Notification
        $users = User::permission('can access users')->get();
        foreach($users as $user) {
            Mail::to("$user->email")->send(new NotificationMailler($request->message));
        }
   }
}
