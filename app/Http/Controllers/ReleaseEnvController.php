<?php

namespace App\Http\Controllers;

use App\Models\ReleaseEnv;
use App\Http\Requests\StoreReleaseEnvRequest;
use App\Http\Requests\UpdateReleaseEnvRequest;

class ReleaseEnvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReleaseEnvRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReleaseEnvRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReleaseEnv  $releaseEnv
     * @return \Illuminate\Http\Response
     */
    public function show(ReleaseEnv $releaseEnv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReleaseEnv  $releaseEnv
     * @return \Illuminate\Http\Response
     */
    public function edit(ReleaseEnv $releaseEnv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReleaseEnvRequest  $request
     * @param  \App\Models\ReleaseEnv  $releaseEnv
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReleaseEnvRequest $request, ReleaseEnv $releaseEnv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReleaseEnv  $releaseEnv
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReleaseEnv $releaseEnv)
    {
        //
    }
}
