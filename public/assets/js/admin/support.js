// Fetch a Data
let fetchdata = () => {
    var html;
    $.ajax({
        type: "get",
        url: "/fetchsupport",
        success: function (response) {
            if(response.support == "") {
                var html = '<tr>\
                <td colspan="6" class="text-center bg-light text-danger">No Records Found</td>\
                </tr>'
                $('#merge-table').html(html);
            }
            $.each(response.support, function (i, val) {
                html += `<tr id=${val.id}>\
                            <td><input type='checkbox' name="check"  value="${val.id}"></td>\
                            <td>${val.notes}</td>\
                            <td>${val.status.name }</td>\
                            <td>${val.project.name}</td>\
                            <td>${val.releaseenv.name }</td>\
                            <td>${val.issue_url}</td>\
                            <td>${val.priority.name}</td>\
                            </tr>`
            });
            $('#merge-table').html(html);
        }
    });
    $("#selectid").click(function () {
        $("input[name='check']").prop('checked', $(this).prop('checked'));
    })
};
fetchdata();
//show
$("#support-send").submit(function(e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var formData = new FormData(this);
    $.ajax({
        type: "post",
        url: "/addSupport",
        data: formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            $('#addsupport').modal('hide');
            fetchdata();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request is Created Successfully");
            $("#notes-support").val(''),
            $("#status-support").val(''),
            $("#project-support").val(''),
            $("#releaseenv-support").val(''),
            $("#issueurl-support").val(''),
            $("#priority-support").val(''),
            $("#file-support").val('')
        },
    });
});


// deleteRecord
$('#delete-support').click(function (e) {
    e.preventDefault();
    var checked = [];
    $('input[name="check"]:checked').each(function () {
        var val = this.value;
        checked.push(val);
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/deleteSupport",
        data: { support: checked },
        dataType: "json",
        success: function (response) {
            $('#merge-table').html("");
            fetchdata();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Support Request Details is Deleted Successfully");
        }
    });
})
// EDIT Merge

$('#edit-support').click(function (e) {
    e.preventDefault();
    if($('input[name="check"]').is(':checked')) {
        var checked = [];
        $('input[name="check"]:checked').each(function () {
            var val = this.value;
            checked.push(val);
        });
        if(checked.length == 1) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "/editSupport",
                data:{ support: checked },
                dataType: "json",
                success: function (response) {
                    $("#id_edit_support").val(response.support[0].id);
                    $("#notes-edit").val(response.support[0].notes);
                    $("#status-edit").val(response.support[0].status_id);
                    $("#project-edit").val(response.support[0].project_id);
                    $("#releaseenv-edit").val(response.support[0].releaseenv_id);
                    $("#priority-edit").val(response.support[0].priority_id );
                    $("#issueurl-edit").val(response.support[0].issue_url);
                    $("#file-edit").val(response.support[0].Filepath);

                    $.each(response.project, function (i, val) {
                        if (response.support[0].project_id == val.name) {
                           $("#select_pro"+val.id).attr('selected', 'selected');
                        }
                    });
                    $.each(response.statu, function (i, val) {
                        if (response.support[0].status_id  == val.name) {
                           $("#select_status"+val.id).attr('selected', 'selected');
                        }
                    });
                    $.each(response.priority, function (i, val) {
                        if (response.support[0].priority_id   == val.name) {
                           $("#select_priority"+val.id).attr('selected', 'selected');
                        }
                    });
                    $.each(response.releaseenv, function (i, val) {
                        if (response.support[0].releaseenv_id    == val.name) {
                           $("#select_env"+val.id).attr('selected', 'selected');
                        }
                    });
                },
            });
            $("#edit-support").attr("data-target","#editsupport");
    }
    else{

        $("#edit-support").removeAttr("data-target","#editsupport");
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Anyone Support Request To Edit");
    }
    }else{
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Support Request To Edit");
    }
        });

 //update
    $('#updated-support').submit(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var formData = new FormData(this);

        // var data = {
        //         notes: $("#notes-edit").val(),
        //         status: $("#status-edit").val(),
        //         project: $("#project-edit").val(),
        //         releaseenv: $("#releaseenv-edit").val(),
        //         priority: $("#priority-edit").val(),
        //         issueurl: $("#issueurl-edit").val(),
        //         // file: $('#file-edit').val(),
        //     };

            var id = $('#id_edit_support').val();
        $.ajax({
            type: "post",
            url: "/updateSupport/"+id,
            data: formData,
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                $('#editsupport').modal('hide');
                fetchdata();
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Support Request Details is Updated Successfully");
              var html = '<td><input type="checkbox" name="check"  value="'+response.support.id+'"></td>\
                            <td>'+response.support.notes+'</td>\
                            <td>'+response.support.status_id+'</td>\
                            <td>'+response.support.project_id+'</td>\
                            <td>'+response.support.releaseenv.name+'</td>\
                            <td>'+response.support.issue_url+'</td>\
                            <td>'+response.support.priority.name+'</td>'
                            $('#merge-table > #'+id).html(html);


            },
        });

    });

//view
$('#view_supportrequests').click(function (e) {
    if($('input[name="check"]').is(':checked')) {
    var checked = [];
    $('input[name="check"]:checked').each(function () {
        var val = this.value;
        checked.push(val);
    });
    if(checked.length == 1) {
       $('#view_supportrequests').attr("href", "view_supportrequests/"+checked);
    }
    else {
        $('#view_supportrequests').removeAttr("href", "view_supportrequests/"+checked);
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Anyone support Request To View");
    }
    }
    else {
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select support Request To View");
    }
});
