// For View Permission
let viewPermission = (id) => {
    $.ajax({
    type: "get",
    url: "/viewPermission/"+id,
    success: function (response) {
        $("#permission_name").html(response.permission.name);
        $('#id_delete_permission').val(id);
    }
    });
}
// Fetch a Data
let fetchdata = () => {
    $.ajax({
        type: "get",
        url: "/fetchPermission",
        success: function (response) {
            if (response.permissions == "") {
                var html = '<tr>\
                            <td colspan="4" class="text-center bg-light text-danger">No Records Found</td>\
                            </tr>'
                $('#permissions_table').append(html);
            }
            $.each(response.permissions, function (i, val) {
            if(response.permission == "can delete permissions") {
            var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.name+'</td>\
                            <td>'+val.guard_name+'</td>\
                            <td>\
                            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-permission" onclick="viewPermission('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                            </td>\
                            </tr>'
            }
            else {
                var html = '<tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.guard_name+'</td>\
                </tr>'
            }
            $('#permissions_table').append(html);
            });
        }
    });
}
fetchdata();
// For Delete Permission
let deletePermission = () => {
    var id = $('#id_delete_permission').val();
    $.ajax({
        type: "get",
        url: "/deletePermission/"+id,
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-permission').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Permission is Deleted Successfully");
                $('#permissions_table').html("");
                fetchdata();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// For Create Permission
$("#savePermission").click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
            name:$("#name").val(),
            guard_name:$("#guard_name").val(),
    };
    $.ajax({
        type: "post",
        url: "/addPermission",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#savePermission').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', ' 73%');
            $('#savePermission').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-permission').modal('hide');
            $("#name").val(''),
            $("#guard_name").val(''),
            $('#name_small').html('');
            $('#guard_small').html('');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Permission is Added Successfully");
                $('#permissions_table').html('');
                fetchdata();
            }
            $('#savePermission').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#savePermission').removeAttr('disabled', 'disabled');
        },
        error:function(error){
            $('#name_small').html(error.responseJSON.errors.name);
            $('#guard_small').html(error.responseJSON.errors.guard_name);
            setTimeout(() => {
                $('#name_small').html('');
                $('#guard_small').html('');
            }, 5000);
            $('#savePermission').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#savePermission').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $("#name").val(''),
    $("#guard_name").val(''),
    $('#name_small').html('');
    $('#guard_small').html('');
});
