$('#report-1').on( 'click',function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var dataPoint = [];
    var chart = new CanvasJS.Chart("testchart", {
        backgroundColor: "#e6eefc",
        animationEnabled: true,
        animationDuration: 500,
        title:{
          text: "mergerequest created by the every user",
          fontColor: "#153b61",
        },
        axisY: {
            title: "mergerequest created"
          },
          axisX: {
            title: "users"
          },
        data: [{
                    click: function(e){
                        toastr.options = {
                            "positionClass": "toast-top-right",
                        },
                toastr.info("dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" )},
                type:"column",
                dataPoints:dataPoint,
        }],
      });
      //Render Chart
    $.getJSON("/merg-report/1",function(response){
        $.each(response.dataPoints,function(i,value){
              dataPoint.push({label: value.first_name, y: parseInt(value.Count)})
                chart.render();
        });
      });
});


// graph from dashboard

 let fetchgraph = ()=> {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var dataPoint = [];
    var chart = new CanvasJS.Chart("testchart", {
        backgroundColor: "#e6eefc",
        animationEnabled: true,
        animationDuration: 500,
        title:{
          text: "mergerequest created by the every user",
          fontColor: "#153b61",
        },
        axisY: {
            title: "mergerequest created"
          },
          axisX: {
            title: "users"
          },
        data: [{
                    click: function(e){
                        toastr.options = {
                            "positionClass": "toast-top-right",
                        },
                toastr.info("dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" )},
                type:"column",
                dataPoints:dataPoint,
        }],
      });
      //Render Chart
    $.getJSON("/merg-report/1",function(response){
        $.each(response.dataPoints,function(i,value){
              dataPoint.push({label: value.first_name, y: parseInt(value.Count)})
                chart.render();
        });
      });


};
fetchgraph();
// for reject count //  2  //

$('#report-2').on( 'click',function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var dataPoint = [];
    var chart = new CanvasJS.Chart("testchart", {
        backgroundColor: "#e6eefc",
        animationEnabled: true,
        animationDuration: 500,
        title:{
          text: "No.of  mergerequest rejected",
          fontColor: "#153b61",
        },
        axisY: {
            title: "mergerequest rejected"
          },
          axisX: {
            title: "users"
          },
        data: [{
                    click: function(e){
                        toastr.options = {
                            "positionClass": "toast-top-right",
                        },
                toastr.info(  "dataSeries Event => Type: "+ e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" )},
                type:"column",
                dataPoints:dataPoint,
        }],
      });
      //Render Chart
    $.getJSON("/merg-report/2",function(response){
        $.each(response.dataPoints,function(i,value){
              dataPoint.push({label: value.first_name, y: parseInt(value.reject_count)})
                chart.render();
        });
      });
});

// for test release
$('#report-3').on( 'click',function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var dataPoint = [];
    var chart = new CanvasJS.Chart("testchart", {
        backgroundColor: "#e6eefc",
        animationEnabled: true,
        animationDuration: 500,
        title:{
          text: "mergerequest created for testing",
          fontColor: "#153b61",
        },
        axisY: {
            title: "test release count"
          },
          axisX: {
            title: "projects"
          },
        data: [{
                    click: function(e){
                        toastr.options = {
                            "positionClass": "toast-top-right",
                        },
                toastr.info(  "dataSeries Event => Type: "+ e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" )},
                type:"column",
                dataPoints:dataPoint,
        }],
      });
      //Render Chart
    $.getJSON("/merg-report/3",function(response){
        $.each(response.dataPoints,function(i,value){
              dataPoint.push({label:value.name, y: parseInt(value.Count)})
                chart.render();
        });
      });
});

// for production release

$('#report-4').on( 'click',function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var dataPoint = [];
    var chart = new CanvasJS.Chart("testchart", {
        backgroundColor: "#e6eefc",
        animationEnabled: true,
        animationDuration: 500,
        title:{
          text: "mergerequest created for production",
          fontColor: "#153b61",
        },
        axisY: {
            title: "production release count"
          },
          axisX: {
            title: "projects"
          },
        data: [{
                    click: function(e){
                        toastr.options = {
                            "positionClass": "toast-top-right",
                        },
                toastr.info(  "dataSeries Event => Type: "+ e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" )},
                type:"column",
                dataPoints:dataPoint,
        }],
      });
      //Render Chart
    $.getJSON("/merg-report/4",function(response){
        $.each(response.dataPoints,function(i,value){
              dataPoint.push({label: value.name, y: parseInt(value.Count)})
                chart.render();
        });
      });
});



