let fetchPriorityId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchPriorityId/"+id ,
        success:function (response){
        // for delete Priority
        $('#id_delete_priority').val(id);
        $('#user_name').html(response.priority.name);
       // End Delete Priority
        }
    });
}
// to showing the priority
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/priority",
    success: function (response) {
        if (response.priority == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#priority-table').append(html);
        }
        $.each(response.priority, function (i, val) {
            if(response.permission == "can delete priority") {
            var html = ' <tr id='+val.id+'>\
            <td>'+val.id+'</td>\
            <td>'+val.name+'</td>\
            <td>'+val.description+'</td>\
            <td>'+val.users.first_name+'</td>\
            <td>\
            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-priority" onclick="fetchPriorityId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
            </td>\
             </tr>';
            }
            else{
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
            </tr>';
            }
$('#priority-table').append(html);
});
}
});
}
dataload();
// for deleting the priority
let deletePriority = () => {
    var id = $('#id_delete_priority').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-priority/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-priority').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Priority is Deleted Successfully");
                $('#priority-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the priority
$('#save-priority').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
       priority_name: $('#name').val(),
       priority_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-priority",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-priority').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-priority').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-priority').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_priority_name").html("");
            $("#small_priority_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Priority is Created Successfully");
                $('#priority-table').html(''),
                dataload();
            }
            $('#save-priority').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-priority').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_priority_name").html(error.responseJSON.errors.priority_name);
            $("#small_priority_des").html(error.responseJSON.errors.priority_description);
            setTimeout(() => {
                $("#small_priority_name").html("");
                $("#small_priority_des").html("");
            }, 5000);
            $('#save-priority').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-priority').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_priority_name").html("");
    $("#small_priority_des").html("");
});
