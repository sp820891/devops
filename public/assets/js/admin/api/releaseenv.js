let fetchEnvId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchEnvId/"+id ,
        success:function (response){
        // for delete release env
        $('#id_delete_env').val(id);
        $('#user_name').html(response.env.name);
       // End Delete release env
        }
    });
}
// to showing the release env
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/releaseenv",
    success: function (response) {
        if (response.envs == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#releaseenv-table').append(html);
        }
        $.each(response.envs, function (i, val) {
            if(response.permission == "can delete release env") {
                    var html = ' <tr id='+val.id+'>\
                    <td>'+val.id+'</td>\
                    <td>'+val.name+'</td>\
                    <td>'+val.description+'</td>\
                    <td>'+val.users.first_name+'</td>\
                    <td>\
                    <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-env" onclick="fetchEnvId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                    </td>\
                </tr>';
            }
            else {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
            </tr>';
            }
$('#releaseenv-table').append(html);
});
}
});
}
dataload();
// for deleting the release env
let deleteEnv = () => {
    var id = $('#id_delete_env').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-releaseenv/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-env').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Env is Deleted Successfully");
                $('#releaseenv-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the release env
$('#save-releaseenv').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        release_env_name: $('#name').val(),
        release_env_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-releaseenv",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-releaseenv').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-releaseenv').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-env').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_env_name").html("");
            $("#small_env_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Env is Created Successfully");
                $('#releaseenv-table').html(''),
                dataload();
            }
            $('#save-releaseenv').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-releaseenv').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_env_name").html(error.responseJSON.errors.release_env_name);
            $("#small_env_des").html(error.responseJSON.errors.release_env_description);
            setTimeout(() => {
                $("#small_env_name").html("");
                $("#small_env_des").html("");
            }, 5000);
            $('#save-releaseenv').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-releaseenv').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_env_name").html("");
    $("#small_env_des").html("");
});
