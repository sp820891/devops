let fetchProjectId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchProjectId/"+id ,
        success:function (response){
        // for delete Project
        $('#id_delete_project').val(id);
        $('#user_name').html(response.project.name);
       // End Delete Project
        }
    });
}
// to showing the project
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/project",
    success: function (response) {
        if (response.projects == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#project-table').append(html);
        }
        $.each(response.projects, function (i, val) {
            if(response.permission == "can delete a project") {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
                <td>\
                <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-project" onclick="fetchProjectId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                </td>\
            </tr>';
            }
            else {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
                </tr>';
            }
$('#project-table').append(html);
});
}
});
}
dataload();
// for deleting the project
let deleteProject = () => {
    var id = $('#id_delete_project').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-project/" + id ,
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-project').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Project is Deleted Successfully");
                $('#project-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the project
$('#save-project').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        project_name: $('#project_name').val(),
        project_description: $('#project_des').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-project",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-project').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-project').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-project').modal('hide');
            $('#project_name').val(''),
            $('#project_des').val(''),
            $("#small_project_name").html("");
            $("#small_project_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Project is Created Successfully");
                $('#project-table').html(''),
                dataload();
            }
            $('#save-project').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-project').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_project_name").html(error.responseJSON.errors.project_name);
            $("#small_project_des").html(error.responseJSON.errors.project_description);
            setTimeout(() => {
                $("#small_project_name").html("");
                $("#small_project_des").html("");
            }, 5000);
            $('#save-project').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-project').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#project_name').val(''),
    $('#project_des').val(''),
    $("#small_project_name").html("");
    $("#small_project_des").html("");
});
