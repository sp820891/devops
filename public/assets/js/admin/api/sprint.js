//For Fetch Sprint Id
let fetchSprintId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchSprintId/"+id ,
        success:function (response){
        // for delete sprint
        $('#id_delete_sprint').val(id);
        $('#user_name').html(response.sprint.name);
       // End Delete sprint
        }
    });
}
// to showing the sprint
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/sprint",
    success: function (response) {
        if (response.sprints == "") {
            var html = '<tr>\
                        <td colspan="7" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#sprint-table').append(html);
        }
        $.each(response.sprints, function (i,val) {
            if(response.permission == "can delete sprint") {
                var  html =' <tr>\
                        <td>'+val.id+'</td>\
                        <td>Week '+val.name+'</td>\
                        <td>'+val.start_date+'</td>\
                        <td>'+val.end_date+'</td>\
                        <td>'+val.goal+'</td>\
                        <td>'+val.users.first_name+'</td>\
                        <td>\
                        <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-sprint" onclick="fetchSprintId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                        </td>\
                    </tr>'
            }
            else {
                var  html =' <tr>\
                        <td>'+val.id+'</td>\
                        <td>Week '+val.name+'</td>\
                        <td>'+val.start_date+'</td>\
                        <td>'+val.end_date+'</td>\
                        <td>'+val.goal+'</td>\
                        <td>'+val.users.first_name+'</td>\
                    </tr>'
            }
            $('#sprint-table').append(html);
        });

    }
});
}
dataload();
// for delete the sprint
let deleteSprint = () => {
    var id = $('#id_delete_sprint').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-sprint/"+id+"/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-sprint').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Sprint is Deleted Successfully");
                $('#sprint-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the sprint
$('#save-sprint').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        sprint:$('#name').val(),
        start_date:$('#start_date').val(),
        end_date:$('#end_date').val(),
        goal:$('#goal').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-sprint",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-sprint').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-sprint').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-sprint').modal('hide');
            $('#name').val("");
            $('#start_date').val("");
            $('#end_date').val("");
            $('#goal').val("");
            $("#name_small").html("");
            $("#start_small").html("");
            $("#end_small").html("");
            $("#goal_small").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Sprint is Created Successfully");
                $('#sprint-table').html("");
                dataload();
            }
            $('#save-sprint').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-sprint').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#name_small").html(error.responseJSON.errors.sprint);
            $("#start_small").html(error.responseJSON.errors.start_date);
            $("#end_small").html(error.responseJSON.errors.end_date);
            $("#goal_small").html(error.responseJSON.errors.goal);
            setTimeout(() => {
                $("#name_small").html("");
                $("#start_small").html("");
                $("#end_small").html("");
                $("#goal_small").html("");
            }, 5000);
            $('#save-sprint').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-sprint').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val("");
    $('#start_date').val("");
    $('#end_date').val("");
    $('#goal').val("");
    $("#name_small").html("");
    $("#start_small").html("");
    $("#end_small").html("");
    $("#goal_small").html("");
});
