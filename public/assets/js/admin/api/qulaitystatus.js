let fetchQtyStatusId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchQtyStatusId/"+id ,
        success:function (response){
        // for delete quality status
        $('#id_delete_qtystatus').val(id);
        $('#user_name').html(response.qty_status.name);
       // End Delete quality status
        }
    });
}
// to showing the quality status
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/qualitystatus",
    success: function (response) {
        if (response.qty_status == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#qualitystatus-table').append(html);
        }
        $.each(response.qty_status, function (i, val) {
            if(response.permission == "can delete quality status") {
            var html = ' <tr id='+val.id+'>\
            <td>'+val.id+'</td>\
            <td>'+val.name+'</td>\
            <td>'+val.description+'</td>\
            <td>'+val.users.first_name+'</td>\
            <td>\
            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-qtystatus" onclick="fetchQtyStatusId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
            </td>\
            </tr>';
            }
            else {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
                </tr>';
            }
$('#qualitystatus-table').append(html);
});
}
});
}
dataload();
// for deleting the quality status
let deleteQtyStatus = () => {
    var id = $('#id_delete_qtystatus').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "api/delete-qualitystatus/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-qtystatus').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Quality Status is Deleted Successfully");
                $('#qualitystatus-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the quality status
$('#save-qualitystatus').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        quality_status_name: $('#name').val(),
        quality_status_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-qualitystatus",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-qualitystatus').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-qualitystatus').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-qtystatus').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_qtystatus_name").html("");
            $("#small_qtystatus_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Quality Status is Created Successfully");
                $('#qualitystatus-table').html(''),
                dataload();
            }
            $('#save-qualitystatus').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-qualitystatus').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_qtystatus_name").html(error.responseJSON.errors.quality_status_name);
            $("#small_qtystatus_des").html(error.responseJSON.errors.quality_status_description);
            setTimeout(() => {
                $("#small_qtystatus_name").html("");
                $("#small_qtystatus_des").html("");
            }, 5000);
            $('#save-qualitystatus').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-qualitystatus').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_qtystatus_name").html("");
    $("#small_qtystatus_des").html("");
});
