let fetchStatusId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchStatusId/"+id ,
        success:function (response){
        // for delete status
        $('#id_delete_status').val(id);
        $('#user_name').html(response.statuses.name);
       // End Delete status
        }
    });
}
// to showing the status
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/status",
    success: function (response) {
        if (response.statuses == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#status-table').append(html);
        }
        $.each(response.statuses, function (i, val) {
            if(response.permission == "can delete status") {
            var html = ' <tr id='+val.id+'>\
            <td>'+val.id+'</td>\
            <td>'+val.name+'</td>\
            <td>'+val.description+'</td>\
            <td>'+val.users.first_name+'</td>\
            <td>\
            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-status" onclick="fetchStatusId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
            </td>\
        </tr>';
            }
            else {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
            </tr>';
            }
$('#status-table').append(html);
});
}
});
}
dataload();
// for deleting the status
let deleteStatus = () => {
    var id = $('#id_delete_status').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "api/delete-status/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-status').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status is Deleted Successfully");
                $('#status-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the status
$('#save-status').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
        status_name: $('#name').val(),
        status_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-status",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-status').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-status').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-status').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_status_name").html("");
            $("#small_status_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Status is Created Successfully");
                $('#status-table').html(''),
                dataload();
            }
            $('#save-status').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-status').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_status_name").html(error.responseJSON.errors.status_name);
            $("#small_status_des").html(error.responseJSON.errors.status_description);
            setTimeout(() => {
                $("#small_status_name").html("");
                $("#small_status_des").html("");
            }, 5000);
            $('#save-status').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-status').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_status_name").html("");
    $("#small_status_des").html("");
});
