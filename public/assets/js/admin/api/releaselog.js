let fetchLogsId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchLogsId/"+id ,
        success:function (response){
        // for delete release logs
        $('#id_delete_log').val(id);
        $('#user_name').html(response.logs.id);
       // End Delete release logs
        }
    });
}
// for deleting the release logs
let deleteLog = () => {
    var id = $('#id_delete_log').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-releaselog/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-log').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Release Log is Deleted Successfully");
                $('#releaselog-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
function dataload(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $.ajax({
        type: "get",
        url: "/api/releaselog",
        success: function (response) {
            if(response.logs == "") {
                var html = '<tr>\
                <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                </tr>'
                $('#releaselog-table').append(html);
            }
            $.each(response.logs, function (i,val) {
                if(response.permission == "can delete release logs") {
                    $('#releaselog-table').append(
                    ' <tr>\
                    <td>'+val.id+'</td>\
                    <td>'+val.mergerequest.git_test_link+'</td>\
                    <td>'+val.mergerequest.issue_url+'</td>\
                    <td>'+val.users.first_name+'</td>\
                    <td>\
                        <a data-toggle="modal" style="cursor:pointer;" data-target="#delete-log" onclick="fetchLogsId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                    </td>\
                </tr>');
                }
                else {
                    $('#releaselog-table').append(
                        ' <tr>\
                        <td>'+val.id+'</td>\
                        <td>'+val.mergerequest.git_test_link+'</td>\
                        <td>'+val.mergerequest.issue_url+'</td>\
                        <td>'+val.users.first_name+'</td>\
                    </tr>');
                }
            });

        }
    });
}

dataload();

