let fetchDepStatusId =(id)=>{
    $.ajax({
        type:"get",
        url:"/api/fetchDepStatusId/"+id ,
        success:function (response){
        // for delete deployment status
        $('#id_delete_depstatus').val(id);
        $('#user_name').html(response.dep_status.name);
       // End Delete deployment status
        }
    });
}
// to showing the deployment status
let dataload = () => {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$.ajax({
    type: "get",
    url: "/api/deploymentstatus",
    success: function (response) {
        if (response.dep_status == "") {
            var html = '<tr>\
                        <td colspan="5" class="text-center bg-light text-danger">No Records Found</td>\
                        </tr>'
            $('#deployment-table').append(html);
        }
        $.each(response.dep_status, function (i, val) {
            if(response.permission == "can delete deployment status") {
            var html = ' <tr id='+val.id+'>\
            <td>'+val.id+'</td>\
            <td>'+val.name+'</td>\
            <td>'+val.description+'</td>\
            <td>'+val.users.first_name+'</td>\
            <td>\
            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-depstatus" onclick="fetchDepStatusId('+val.id+')"><i class="ti-trash text-danger"></i></a>\
            </td>\
        </tr>';
            }
            else {
                var html = ' <tr id='+val.id+'>\
                <td>'+val.id+'</td>\
                <td>'+val.name+'</td>\
                <td>'+val.description+'</td>\
                <td>'+val.users.first_name+'</td>\
            </tr>';
            }
$('#deployment-table').append(html);
});
}
});
}
dataload();
// for deleting the deployment status
let deleteDepStatus = () => {
    var id = $('#id_delete_depstatus').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "delete",
        url: "/api/delete-deploymentstatus/" + id + "/delete",
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-depstatus').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Deployment Status is Deleted Successfully");
                $('#deployment-table').html("");
                dataload();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// for storing the deployment status
$('#save-depstatus').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
      deployment_status_name: $('#name').val(),
      deployment_status_description: $('#description').val(),
    }
    $.ajax({
        type: "post",
        url: "/api/store-deploymentstatus",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#save-depstatus').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#save-depstatus').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-depstatus').modal('hide');
            $('#name').val(''),
            $('#description').val(''),
            $("#small_depstatus_name").html("");
            $("#small_depstatus_des").html("");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Deployment Status is Created Successfully");
                $('#deployment-table').html(''),
                dataload();
            }
            $('#save-depstatus').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-depstatus').removeAttr('disabled', 'disabled');
        },
        error: function(error) {
            $("#small_depstatus_name").html(error.responseJSON.errors.deployment_status_name);
            $("#small_depstatus_des").html(error.responseJSON.errors.deployment_status_description);
            setTimeout(() => {
                $("#small_depstatus_name").html("");
                $("#small_depstatus_des").html("");
            }, 5000);
            $('#save-depstatus').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#save-depstatus').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#name').val(''),
    $('#description').val(''),
    $("#small_depstatus_name").html("");
    $("#small_depstatus_des").html("");
});
