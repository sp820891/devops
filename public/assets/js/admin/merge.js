let fetchDeleteMerge = (id,name) => {
    $('#id_delete_merge').val(id);
    $('#user_name').html(name);
}
// // Fetch a Data
let fetchdata = () => {
    $.ajax({
        type: "get",
        url: "/fetchMerge",
        success: function (response) {
            if(response.merge == "") {
                var html = '<tr>\
                <td colspan="6" class="text-center bg-light text-danger">No Records Found</td>\
                </tr>'
                $('#merge-table').append(html);
            }
            $.each(response.merge, function (i, val) {
                var html = `<tr id=${val.id}>\
                            <td><input type='checkbox' name="check"  value="${val.id}"></td>
                            <td id='td'>${val.projects.name}</td>
                            <td id='td'>${val.issue_url}</td>
                            <td id='td'>${val.git_test_link}</td>
                            <td id='td'>${val.git_prod_link}</td>
                            <td id='td'>${val.user.first_name}</td>
                            <td>
                            <a class="text-primary"><i class="ti-pencil-alt"></i></a>&nbsp;&nbsp;&nbsp;
                            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-mergerequest" onclick="fetchDeleteMerge(${val.id},'${val.user.first_name}')"
                             class="text-danger"><i class="ti-trash"></i></a>
                            </td>
                            </tr>`
                $('#merge-table').append(html);
                $('#'+val.id+'> #td').click(function() {
                    return false;
                }).dblclick(function() {
                    window.location = `view_mergerequests/${val.id}`
                    return false;
                });
            });


        }


    });

    $("#selectid").click(function () {
        $("input[name='check']").prop('checked', $(this).prop('checked'));
    })
};
fetchdata();



// For Create User
$("#merge_send").submit( function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
 var formData = new  FormData(this);


    $.ajax({
        type: "post",
        url: "/addMerge",
        data:formData,
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('#addmerge').modal('hide');
            $("#email").val('');
            $("#git_test_link").val('');
            $("#sqllink").val('');
            $("#envlink").val('');
            $('#priority').val('');
            $('#qscore').val('');
            $('#artisan').val('');
            $('#issue_url').val('');
            $('#depstatus').val('');
            $('#envlink').val('');
            $('#attach').val('');
            $('#merge-table').html("");
            fetchdata();
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request Created Successfully");
            var notify = {
                message:"New Merge Request is Created and its Url is "+response.url
            };
            $.ajax({
                type: "post",
                url: "/mailAndNotification",
                data: notify,
                dataType: "json",
            });
        },
        error: function (error) {
            $('#test_small').html(error.responseJSON.errors.git_test_link);
            $('#deps_small').html(error.responseJSON.errors.Deployment_Status);
            $('#issueurl_small').html(error.responseJSON.errors.issue_url);
            setTimeout(() => {
                $('#test_small').html('');
                $('#deps_small').html('');
                $('#issueurl_small').html('');
            }, 5000);
        }

    });
});




let deleteMerge = () => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/deleteMerge",
        data: { merge:[$('#id_delete_merge').val()]},
        dataType: "json",
        success: function (response) {
            $('#delete-mergerequest').modal('hide');
            $('#merge-table').html("");
            fetchdata();
            toastr.options = {

                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request Details is Deleted Successfully");
        }
    });
}
// deleteRecord Multiple
$('#delete-merge').click(function (e) {
    e.preventDefault();
    if($('input[name="check"]').is(':checked')) {
    var checked = [];
    $('input[name="check"]:checked').each(function () {
        var val = this.value;
        checked.push(val);
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/deleteMerge",
        data: { merge: checked },
        dataType: "json",
        success: function (response) {
            $('#merge-table').html("");
            fetchdata();
            toastr.options = {

                "positionClass": "toast-bottom-right",
            }
            toastr.success("Merge Request Details is Deleted Successfully");
        }
    });
    $('#delete-merge').attr("href", "deleteMerge");
    }
    else {
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Merge Request To Delete");
    }
});

//view
$('#view_mergerequests').click(function (e) {
    if($('input[name="check"]').is(':checked')) {
    var checked = [];
    $('input[name="check"]:checked').each(function () {
        var val = this.value;
        checked.push(val);
    });
    if(checked.length == 1) {
       $('#view_mergerequests').attr("href", "view_mergerequests/"+checked);
    }
    else {
        $('#view_mergerequests').removeAttr("href", "view_mergerequests/"+checked);
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Anyone Merge Request To View");
    }
    }
    else {
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Merge Request To View");
    }
});

// EDIT Merge
$('#edit-merge').click(function (e) {
    e.preventDefault();
    if($('input[name="check"]').is(':checked')) {
    var checked = [];
    $('input[name="check"]:checked').each(function () {
        var val = this.value;
        checked.push(val);
    });
    if(checked.length == 1) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "/editMerge",
        data: { merge: checked },
        dataType: "json",
        success: function (response) {
            $("#testlink_edit").val(response.merge[0].git_test_link);
            $("#projectedit").val(response.merge[0].project_id);
            $("#prodlink_edit").val(response.merge[0].git_prod_link);
            $("#sqllink_edit").val(response.merge[0].sql_update_link);
            $("#envlink_edit").val(response.merge[0].env_update_link);
            $("#artisan_edit").val(response.merge[0].artisan_command);
            $("#issueurl_edit").val(response.merge[0].issue_url);
            $("#depstatus_edit").val(response.merge[0].depstatus_id);
            $("#qscore_edit").val(response.merge[0].quality_score);
            $("#releaseenv_edit").val(response.merge[0].releaseenv_id);
            $("#priority_edit").val(response.merge[0].priority_id);
            $("#status_edit").val(response.merge[0].status_id);
            $("#reject_edit").val(response.merge[0].reject_count);
            $("#id_edit_merge").val(response.merge[0].id);



            $.each(response.sprint, function (i, val) {
                if (response.merge[0].sprint_id == val.name) {
                   $("#select_sprint"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.project, function (i, val) {
                if (response.merge[0].project_id == val.name) {
                   $("#select_project"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.depstatus, function (i, val) {
                if (response.merge[0].depstatus_id == val.name) {
                   $("#select_depstatus"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.statuses, function (i, val) {
                if (response.merge[0].status_id  == val.name) {
                   $("#select_status"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.priority, function (i, val) {
                if (response.merge[0].priority_id   == val.name) {
                   $("#select_priority"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.releaseenv, function (i, val) {
                if (response.merge[0].releaseenv_id    == val.name) {
                   $("#select_env"+val.id).attr('selected', 'selected');
                }
            });
            $.each(response.quality, function (i, val) {
                if (response.merge[0].qualitystatus_id  == val.name) {
                   $("#select_quality"+val.id).attr('selected', 'selected');
                }
            });


        },

        });
        $("#edit-merge").attr("data-target","#editmerge");
    }
    else {
        $("#edit-merge").removeAttr("data-target","#editmerge");
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Anyone Merge Request To Edit");
    }
    }
    else {
        toastr.options = {

            "positionClass": "toast-bottom-right",
        }
        toastr.error("Please Select Merge Request To Edit");
    }
    });
        var fetchComment = () => {
            var id = $('#merge_id').val();
            var name =$('#user_name').val();
                $.ajax({
                    type: "get",
                    url: "/showComment/"+id,
                    success: function (response) {
                        //console.log(response.created_at);
                        $.each(response.comment, function (i, val) {
                            var dt = new Date(val.created_at);
                            var dd = dt.toDateString()+" "+dt.getHours()+":"+dt.getMinutes();
                            // var time = dt.getHours() + ":" + dt.getMinutes();
                            //console.log(time);
                            // if(response.status == 404) {
                            //     var html = `Start Conversation`;
                            // }
                            // else {
                                    if(response.auth == val.users.first_name) {
                                        var html = `<div class="d-flex"><h6 style="color: rgb(10, 14, 117)";>You&nbsp</h6><small>${dd}</small></div><p class="text-wrap" style="color: rgb(0, 0, 0);">${val.comment}</p> `;
                                    }
                                    else {
                                        var html = `<div class="d-flex"><h6 style="color: rgb(10, 14, 117)";>${val.users.first_name}&nbsp</h6><small>${dd}</small></div><p class="text-wrap" style="color: rgb(0, 0, 0);">${val.comment}</p> `;
                                    }
                                    $("#comments_view").append(html);
                            // }
                        });
                    }

                });
        }
    fetchComment();
    $('#up-comment').click(function (e) {
        e.preventDefault();
        if($("#casual_comment").val() == "") {
            toastr.options = {
                "positionClass": "toast-bottom-right",
            }
            toastr.error("Please Enter Comment");
        }
        else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var data ={
            comment: $("#casual_comment").val(),
        };
        var id = $('#merge_id').val();
        $.ajax({
            type: "post",
            url: "/updateComment/"+id,
            data: data,
            dataType: "json",
            success: function (response) {
                $("#comments_view").html("");
                fetchComment();
                $("#casual_comment").val("");

            },
        });
    }
    })

    //Update Merge
    $('#update-merge').submit(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = new  FormData(this);
        var id = $('#id_edit_merge').val();
        $.ajax({
            type: "post",
            url: "/updateMerge/"+id,
            data: formData,
            dataType: "json",
            contentType: false,
        cache: false,
        processData: false,
            success: function (response) {
                var html = '<td><input type="checkbox" name="check"  value="'+response.merge.id+'"></td>\
                            <td>'+response.merge.projects.name+'</td>\
                            <td>'+response.merge.issue_url+'</td>\
                            <td>'+response.merge.git_test_link+'</td>\
                            <td>'+response.merge.git_prod_link+'</td>\
                            <td>'+response.merge.artisan_command+'</td>'
                $('#merge-table > #'+id).html(html);
                $('#editmerge').modal('hide');
                toastr.options = {

                    "positionClass": "toast-bottom-right",

                }
                toastr.success("Merge Request Details is Updated Successfully");
                var notify = {
                    message:"Merge Request is Updated and its Url is "+response.url
                };
                $.ajax({
                    type: "post",
                    url: "/mailAndNotification",
                    data: notify,
                    dataType: "json",
                });
                if(response.dep_status == "1") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Test Release Done and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "2") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Production Release Done and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "3") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Reject and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "4") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Test Release Request and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "5") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Production Release Request and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "6") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Failed and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
                else if(response.dep_status == "7") {
                    var notify = {
                        message:"Merge Request Deployment Status is changed to Review and its Url is "+response.url
                    };
                    $.ajax({
                        type: "post",
                        url: "/mailAndNotification",
                        data: notify,
                        dataType: "json",
                    });
                }
            },
        });

    });
