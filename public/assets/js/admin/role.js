// For View Role
let viewRole = (id) => {
    $.ajax({
    type: "get",
    url: "/viewRole/"+id,
    success: function (response) {
        console.log(response.role.name);
        $("#role_name").html(response.role.name);
        $('#id_delete_role').val(id);
    }
    });
}
// Fetch a Data
let fetchdata = () => {
    $.ajax({
        type: "get",
        url: "/fetchRole",
        success: function (response) {
            if (response.roles == "") {
                var html = '<tr>\
                            <td colspan="4" class="text-center bg-light text-danger">No Records Found</td>\
                            </tr>'
                $('#roles_table').append(html);
            }
            $.each(response.roles, function (i, val) {
            if(response.permission1 == "can access role based permissions" && response.permission2 == "can delete roles") {
            var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.name+'</td>\
                            <td>'+val.guard_name+'</td>\
                            <td>\
                            <a href="roleandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp\
                            <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-role" onclick="viewRole('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                            </td>\
                            </tr>'
            }
            else if(response.permission1 == "can access role based permissions") {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.name+'</td>\
                                <td>'+val.guard_name+'</td>\
                                <td>\
                                <a href="roleandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp\
                                </td>\
                                </tr>'
            }
            else if(response.permission2 == "can delete roles") {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.name+'</td>\
                                <td>'+val.guard_name+'</td>\
                                <td>\
                                <a style="cursor:pointer;" data-toggle="modal" data-target="#delete-role" onclick="viewRole('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                                </td>\
                                </tr>'
            }
            else {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.name+'</td>\
                                <td>'+val.guard_name+'</td>\
                                </tr>'
                }
            $('#roles_table').append(html);
            });
        }
    });
}
fetchdata();
//Delete Role
let deleteRole = () => {
    var id = $('#id_delete_role').val();
    $.ajax({
        type: "get",
        url: "/deleteRole/"+id,
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-role').modal('hide');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Role is Deleted Successfully");
                $('#roles_table').html("");
                fetchdata();
            }
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
        }
    });
}
// For Create Role
$("#saveRole").click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
            name:$("#name").val(),
            guard_name:$("#guard_name").val(),
    };
    $.ajax({
        type: "post",
        url: "/addRole",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#saveRole').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.submit-btn').css('margin-left', '73%');
            $('#saveRole').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#add-role').modal('hide');
            $("#name").val(''),
            $("#guard_name").val(''),
            $('#name_small').html('');
            $('#guard_small').html('');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("Role is Added Successfully");
                $('#roles_table').html('');
                fetchdata();
            }
            $('#saveRole').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#saveRole').removeAttr('disabled', 'disabled');
        },
        error:function(error){
            $('#name_small').html(error.responseJSON.errors.name);
            $('#guard_small').html(error.responseJSON.errors.guard_name);
            setTimeout(() => {
                $('#name_small').html('');
                $('#guard_small').html('');
            }, 5000);
            $('#saveRole').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#saveRole').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $("#name").val(''),
    $("#guard_name").val(''),
    $('#name_small').html('');
    $('#guard_small').html('');
});
