// For Edit User
let editUser = (id) => {
    $.ajax({
    type: "get",
    url: "/editUser/"+id,
    success: function (response) {
        $("#first_name_edit").val(response.user.first_name);
        $("#last_name_edit").val(response.user.last_name);
        $("#email_edit").val(response.user.email);
        $('#id_edit_user').val(id);
        //For Delete User
        $('#id_delete_user').val(id);
        $('#deleted_user_email').val(response.user.email);
        $('#user_name').html(response.user.first_name);
        //End For Delete User
        if (response.user.gender == 'Male') {
            $("#gender_edit_male").prop('checked', true);
            $("#gender_edit_female").prop('checked', false);
        } else {
            $("#gender_edit_male").prop('checked', false);
            $("#gender_edit_female").prop('checked', true);
        }
        $.each(response.roles, function (i, val) {
            if (response.role == val.name) {
               $("#roles_option"+val.id).attr('selected', 'selected');
            }
        });
    }
    });
}
// Fetch a Data
let fetchdata = () => {
    $.ajax({
        type: "get",
        url: "/fetchUser",
        success: function (response) {
            if (response.users == "") {
                var html = '<tr>\
                            <td colspan="6" class="text-center bg-light text-danger">No Records Found</td>\
                            </tr>'
                $('#users_table').append(html);
            }
            $.each(response.users, function (i, val) {
            if(response.permission1 == "can access user based permissions" && response.permission2 == "can edit users" && response.permission3 == "can delete users") {
            var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.first_name+'</td>\
                            <td>'+val.gender+'</td>\
                            <td>'+val.email+'</td>\
                            <td>'+val.roles[0].name+'</td>\
                            <td>\
                            <a href="userandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp&nbsp&nbsp\
                            <a data-toggle="modal" style="cursor:pointer;" data-target="#edit-user" onclick="editUser('+val.id+')"><i class="ti-pencil-alt text-primary"></i></span></a>&nbsp&nbsp\
                            <a data-toggle="modal" style="cursor:pointer;" data-target="#delete-user" onclick="editUser('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                            </td>\
                            </tr>'
            }
            else if(response.permission1 == "can access user based permissions" && response.permission2 == "can edit users") {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.first_name+'</td>\
                                <td>'+val.gender+'</td>\
                                <td>'+val.email+'</td>\
                                <td>'+val.roles[0].name+'</td>\
                                <td>\
                                <a href="userandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp&nbsp&nbsp\
                                <a data-toggle="modal" style="cursor:pointer;" data-target="#edit-user" onclick="editUser('+val.id+')"><i class="ti-pencil-alt text-primary"></i></span></a>&nbsp&nbsp\
                                </td>\
                                </tr>'
            }
            else if(response.permission2 == "can edit users" && response.permission3 == "can delete users") {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.first_name+'</td>\
                                <td>'+val.gender+'</td>\
                                <td>'+val.email+'</td>\
                                <td>'+val.roles[0].name+'</td>\
                                <td>\
                                <a data-toggle="modal" style="cursor:pointer;" data-target="#edit-user" onclick="editUser('+val.id+')"><i class="ti-pencil-alt text-primary"></i></span></a>&nbsp&nbsp\
                                <a data-toggle="modal" style="cursor:pointer;" data-target="#delete-user" onclick="editUser('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                                </td>\
                                </tr>'
            }
            else if(response.permission1 == "can access user based permissions" && response.permission3 == "can delete users") {
                var html = '<tr id='+val.id+'>\
                                <td>'+val.id+'</td>\
                                <td>'+val.first_name+'</td>\
                                <td>'+val.gender+'</td>\
                                <td>'+val.email+'</td>\
                                <td>'+val.roles[0].name+'</td>\
                                <td>\
                                <a href="userandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp&nbsp&nbsp\
                                <a data-toggle="modal" style="cursor:pointer;" data-target="#delete-user" onclick="editUser('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                                </td>\
                                </tr>'
            }
            else if(response.permission1 == "can access user based permissions") {
                var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.first_name+'</td>\
                            <td>'+val.gender+'</td>\
                            <td>'+val.email+'</td>\
                            <td>'+val.roles[0].name+'</td>\
                            <td>\
                            <a href="userandpermissions/'+val.id+'"><i class="ti-eye text-warning"></i></a>&nbsp&nbsp&nbsp\
                            </td>\
                            </tr>'
            }
            else if(response.permission2 == "can edit users") {
                var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.first_name+'</td>\
                            <td>'+val.gender+'</td>\
                            <td>'+val.email+'</td>\
                            <td>'+val.roles[0].name+'</td>\
                            <td>\
                            <a data-toggle="modal" style="cursor:pointer;" data-target="#edit-user" onclick="editUser('+val.id+')"><i class="ti-pencil-alt text-primary"></i></span></a>&nbsp&nbsp\
                            </td>\
                            </tr>'
            }
            else if(response.permission3 == "can delete users") {
                var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.first_name+'</td>\
                            <td>'+val.gender+'</td>\
                            <td>'+val.email+'</td>\
                            <td>'+val.roles[0].name+'</td>\
                            <td>\
                            <a data-toggle="modal" style="cursor:pointer;" data-target="#delete-user" onclick="editUser('+val.id+')"><i class="ti-trash text-danger"></i></a>\
                            </td>\
                            </tr>'
            }
            else {
                var html = '<tr id='+val.id+'>\
                            <td>'+val.id+'</td>\
                            <td>'+val.first_name+'</td>\
                            <td>'+val.gender+'</td>\
                            <td>'+val.email+'</td>\
                            <td>'+val.roles[0].name+'</td>\
                            </tr>'
            }
            $('#users_table').append(html);
            });
        }
    });
}
fetchdata();
// For Delete User
let deleteUser = () => {
    var id = $('#id_delete_user').val();
    $.ajax({
        type: "get",
        url: "/deleteUser/"+id,
        beforeSend : function () {
            $('#delete_btn').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#delete_btn').attr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "53%");
        },
        success: function (response) {
            $('#delete-user').modal('hide');
            $('#delete_btn').html('Delete');
            $('#delete_btn').removeAttr('disabled', 'disabled');
            $('.submit_delete_btn').css("margin-left", "67%");
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("User Details is Deleted Successfully");
                $('#users_table').html('');
                fetchdata();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var notify = {
                    message:"User Details is Deleted and Deleted User Email Id is "+$('#deleted_user_email').val()
                };
                $.ajax({
                    type: "post",
                    url: "/mailAndNotification",
                    data: notify,
                    dataType: "json",
                });
            }
        }
    });
}
// For Create User
$("#saveUser").click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
            first_name:$("#first_name").val(),
            last_name:$("#last_name").val(),
            email:$("#email").val(),
            gender:$("input[name=gender]:checked").val(),
            role_as:$("#role_as").val(),
            password:$("#password").val(),
            password_confirmation:$("#password_confirmation").val(),
    };
    $.ajax({
        type: "post",
        url: "/addUser",
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#saveUser').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('#saveUser').attr('disabled', 'disabled');
            $('.submit-btn').css('margin-left', ' 73%');
        },
        success: function (response) {
            $('#add-user').modal('hide');
            $("#first_name").val(''),
            $("#last_name").val(''),
            $("#email").val(''),
            $("input[name=gender]:checked").prop('checked', false),
            $("#role_as").val(''),
            $("#password").val(''),
            $("#password_confirmation").val(''),
            $('#fname_small').html('');
            $('#lname_small').html('');
            $('#gender_small').html('');
            $('#role_small').html('');
            $('#password_small').html('');
            $('#email_small').html('');
            $('#saveUser').html('Save');
            $('#saveUser').removeAttr('disabled', 'disabled');
            $('.submit-btn').css('margin-left', '87%');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("User is Added Successfully");
                $('#users_table').html('');
                fetchdata();
                var credentials = {
                    name:response.name,
                    email:response.email,
                };
                $.ajax({
                    type: "post",
                    url: "/userCredentialMailler",
                    data: credentials,
                    dataType: "json",
                });
                var notify = {
                    message:"New User is Created"
                };
                $.ajax({
                    type: "post",
                    url: "/mailAndNotification",
                    data: notify,
                    dataType: "json",
                });
            }
        },
        error:function(error){
            $('#fname_small').html(error.responseJSON.errors.first_name);
            $('#lname_small').html(error.responseJSON.errors.last_name);
            $('#gender_small').html(error.responseJSON.errors.gender);
            $('#role_small').html(error.responseJSON.errors.role_as);
            $('#password_small').html(error.responseJSON.errors.password);
            $('#email_small').html(error.responseJSON.errors.email);
            setTimeout(() => {
                $('#fname_small').html('');
                $('#lname_small').html('');
                $('#gender_small').html('');
                $('#role_small').html('');
                $('#password_small').html('');
                $('#email_small').html('');
            }, 5000);
            $('#saveUser').html('Save');
            $('.submit-btn').css('margin-left', '87%');
            $('#saveUser').removeAttr('disabled', 'disabled');
        }
    });
});
$('#close').click(function (e) {
    e.preventDefault();
    $("#first_name").val(''),
    $("#last_name").val(''),
    $("#email").val(''),
    $("input[name=gender]:checked").prop('checked', false),
    $("#role_as").val(''),
    $("#password").val(''),
    $("#password_confirmation").val(''),
    $('#fname_small').html('');
    $('#lname_small').html('');
    $('#gender_small').html('');
    $('#role_small').html('');
    $('#password_small').html('');
    $('#email_small').html('');
});
//Update a Data
$('#updateUser').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = {
            first_name:$("#first_name_edit").val(),
            last_name:$("#last_name_edit").val(),
            email:$("#email_edit").val(),
            gender:$("input[name=gender_edit]:checked").val(),
            role_as:$("#role_as_edit").val(),
    };
    var id = $('#id_edit_user').val();
    $.ajax({
        type: "post",
        url: "/updateUser/"+id,
        data: data,
        dataType: "json",
        beforeSend : function () {
            $('#updateUser').html('<i class="fa fa-spinner fa-spin"></i> Please Wait...');
            $('.update-btn').css('margin-left', ' 73%');
            $('#updateUser').attr('disabled', 'disabled');
        },
        success: function (response) {
            $('#updateUser').html('Update');
            $('.update-btn').css('margin-left', ' 84%');
            $('#updateUser').removeAttr('disabled', 'disabled');
            if(response.status == 404) {
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.error("Something Went Wrong");
            }
            else {
                $('#users_table').html('');
                fetchdata();
                $('#edit-user').modal('hide');
                toastr.options = {
                    "positionClass": "toast-bottom-right",
                }
                toastr.success("User Details is Updated Successfully");

                var notify = {
                    message:"User Details is Updated and Updated User Email Id is "+response.email
                };
                $.ajax({
                    type: "post",
                    url: "/mailAndNotification",
                    data: notify,
                    dataType: "json",
                });
            }
        },
        error:function(error){
            $('#fname_small_edit').html(error.responseJSON.errors.first_name);
            $('#lname_small_edit').html(error.responseJSON.errors.last_name);
            $('#gender_small_edit').html(error.responseJSON.errors.gender);
            $('#role_small_edit').html(error.responseJSON.errors.role_as);
            $('#password_small_edit').html(error.responseJSON.errors.password);
            $('#email_small_edit').html(error.responseJSON.errors.email);
            setTimeout(() => {
                $('#fname_small_edit').html('');
                $('#lname_small_edit').html('');
                $('#gender_small_edit').html('');
                $('#role_small_edit').html('');
                $('#password_small_edit').html('');
                $('#email_small_edit').html('');
            }, 5000);
            $('#updateUser').html('Update');
            $('.update-btn').css('margin-left', ' 84%');
            $('#updateUser').removeAttr('disabled', 'disabled');
        }
    });

});
